/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SourceShaderProgram.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 16:47:03 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/14 16:54:51 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SourceShaderProgram.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

SourceShaderProgram::SourceShaderProgram(const std::string vertex,
		const std::string geometry, const std::string fragment) :
	_vertex(vertex), _geometry(geometry), _fragment(fragment)
{
}

SourceShaderProgram::~SourceShaderProgram(void) {}

// Member Functions

const std::string	&SourceShaderProgram::getVertex(void) const
{
	return (this->_vertex);
}

const std::string	&SourceShaderProgram::getGeometry(void) const
{
	return (this->_geometry);
}

const std::string	&SourceShaderProgram::getFragment(void) const
{
	return (this->_fragment);
}

// Print

void				SourceShaderProgram::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "SourceShaderProgram" << C_RESET << " : {" << logger::tab(1);
	o << C_VAR << "vertex" << C_RESET << " = " << this->_vertex << ",\n";
	o << C_VAR << "geometry" << C_RESET << " = " << this->_geometry << ",\n";
	o << C_VAR << "fragment" << C_RESET << " = " << this->_fragment;
	o << logger::tab(-1) << "}";
}

}

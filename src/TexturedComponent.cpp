/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TexturedComponent.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/14 16:16:50 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/16 15:11:01 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "TexturedComponent.hpp"
#include "Scene.hpp"
#include "TexturedComponentVAO.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

TexturedComponent::TexturedComponent(Scene &scene, Area *area,
		const std::string &path, const Vec2 &position, const Vec2 &size,
		const PositionType pos_type, const PositionType size_type,
		const int level)
	throw(ShaderProgram::InvalidShaderProgramException,
			Texture::InvalidFileException) :
	SceneComponent(scene, area, position, size, pos_type, size_type),
	Renderable(scene.getRender(), scene.getCamera(), level)
{
	this->_vao = new TexturedComponentVAO(this->_shaderProgram.getID(),
			this->_position, this->_size, path);
}

TexturedComponent::~TexturedComponent(void)
{
	logger::sslog(LOG_DEBUG) << "Delete TexturedComponent: " << *this << logger::endlog();
	delete this->_vao;
}

// Member Functions

void	TexturedComponent::render(void) const
{
	this->_shaderProgram.use();
	this->_shaderProgram.updateCamera(this->_camera);
	this->_vao->render();
}

void	TexturedComponent::onModif(void)
{
	this->_vao->updatePositions(this->_position, this->_size);
}

// Print

void	TexturedComponent::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "TexturedComponent" << C_RESET << ": {" << logger::tab(1);
	SceneComponent::objectPrint(o);
	o << ",\n";
	Renderable::objectPrint(o);
	o << ",\n";
	o << C_VAR << "shader program" << C_RESET << " = " << this->_shaderProgram << ",\n";
	o << C_VAR << "vao" << C_RESET << " = " << this->_vao;
	o << logger::tab(-1) << "}";
}

}

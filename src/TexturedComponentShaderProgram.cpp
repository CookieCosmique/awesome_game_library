/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TexturedComponentShaderProgram.cpp                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/14 16:09:51 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/16 17:33:01 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "TexturedComponentShaderProgram.hpp"
#include "SourceShaderProgram.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

TexturedComponentShaderProgram::TexturedComponentShaderProgram(void)
	throw(InvalidShaderProgramException) :
		CameraShaderProgram(SourceShaderProgram(
"#version 400 core\n\
\n\
layout(location = 1) in vec2 position; \n\
layout(location = 2) in vec2 UVs; \n\
\n\
out vec2	vUV; \n\
\n\
void	main(void) \n\
{ \n\
	gl_Position = vec4(position, 0.0f, 1.0f); \n\
	vUV = UVs; \n\
}"
					,
"#version 400 core \n\
\n\
layout(triangles) in; \n\
layout(triangle_strip, max_vertices = 3) out; \n\
\n\
in vec2			vUV[];\n\
\n\
out vec2		uv;\n\
\n\
uniform vec2	cam_size;\n\
uniform vec2	cam_pos;\n\
\n\
void	emit_points(vec2 pos, vec2 in_uv)\n\
{\n\
	uv = in_uv;\n\
	gl_Position = vec4((pos.x - cam_pos.x) / cam_size.x * 2 - 1.0f,\n\
						(pos.y - cam_pos.y) / cam_size.y * 2 - 1.0f, 0.0f, 1.0f);\n\
	EmitVertex();\n\
}\n\
\n\
void	main(void)\n\
{\n\
	for (int i = 0; i < 3; i++)\n\
		emit_points(gl_in[i].gl_Position.xy, vUV[i]);\n\
	EndPrimitive();\n\
}"
					,
"#version 400 core\n\
\n\
in vec2				uv;\n\
\n\
out vec4			out_color;\n\
\n\
uniform sampler2D	texture_sampler;\n\
\n\
void	main(void)\n\
{\n\
	out_color = vec4(texture(texture_sampler, uv).rgb, 1.0f);\n\
}"
					))
{

}

TexturedComponentShaderProgram::~TexturedComponentShaderProgram(void)
{

}

// Print

void	TexturedComponentShaderProgram::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "TexturedComponentShaderProgram" << C_RESET << ": {" <<
		logger::tab(1);
	CameraShaderProgram::objectPrint(o);
	o << logger::tab(-1) << "}";
}

}

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   SquareVAO.cpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-16 16:54:17 by Bastien             |\________\          */
/*   Updated: 2016/06/16 15:18:01 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SquareVAO.hpp"
#include "OpenGL.h"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

SquareVAO::SquareVAO(const GLuint program_id, const Vec2 &position,
			const Vec2 &size)
{
	GLfloat		points[8] = {
			position.x, position.y,
			position.x, position.y + size.y,
			position.x + size.x, position.y,
			position.x + size.x, position.y + size.y
	};
	GLuint		indices[6] = {0, 1, 2, 1, 2, 3};

	this->_vbo = loadVBO(program_id, sizeof(GLfloat) * 8, points, GL_FLOAT, 1,
			"position", 2, GL_DYNAMIC_DRAW);
	this->_ebo = loadEBO(sizeof(GLuint) * 6, indices, GL_STATIC_DRAW);
}

SquareVAO::~SquareVAO(void)
{

}

// Member functions

void		SquareVAO::render(void) const
{
	this->bind();
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void		SquareVAO::updatePositions(const Vec2 &position, const Vec2 &size)
{
	GLfloat		points[8] = {
			position.x, position.y,
			position.x, position.y + size.y,
			position.x + size.x, position.y,
			position.x + size.x, position.y + size.y
	};

	updateBufferData(GL_ARRAY_BUFFER, this->_vbo, sizeof(GLfloat) * 8, points);
}

// Print

void		SquareVAO::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "SquareVAO" << C_RESET << ": {" << logger::tab(1);
	VAO::objectPrint(o);
	o << ",\n";
	o << C_VAR << "vbo" << C_RESET << " = " << this->_vbo << ",\n";
	o << C_VAR << "ebo" << C_RESET << " = " << this->_ebo;
	o << logger::tab(-1) << "}";
}

}

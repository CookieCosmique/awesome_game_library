/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Environment.hpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 03:39:04 by Bastien             |\________\          */
/*   Updated: 2016/06/04 15:52:44 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Environment.hpp"
#include "Logger.hpp"
#include "InputHandler.hpp"

namespace agl
{

// Exceptions

// GLFWInitErrorException
const char		*Environment::GLFWInitErrorException::what(void) const throw()
{
	return ("GLFWInitErrorException");
}

// WindowCreationErrorException
const char		*Environment::WindowCreationErrorException::what(void) const throw()
{
	return ("WindowCreationErrorException");
}

// GlewInitErrorException

Environment::GlewInitErrorException::GlewInitErrorException(const char *error) :
	_error(error)
{
}

const char		*Environment::GlewInitErrorException::what(void) const throw()
{
	std::stringstream	ss;

	ss << "GlewInitErrorException: " << this->_error;
	return (ss.str().c_str());
}



// Constructor - Destructor

Environment::Environment(void)
{

}

Environment::~Environment(void)
{
	logger::sslog(LOG_FINE) << "Starting " << C_OBJ << "Environment" <<
		C_RESET << " deletion ..." << logger::endlog(1);
	delete this->_inputs;
	logger::sslog(LOG_FINE, -1) << C_OBJ << "Environment" << C_RESET <<
		" Deleted." << logger::endlog();
}

// Public Member functions

void	Environment::init(void) throw(GLFWInitErrorException,
		WindowCreationErrorException)
{
	logger::sslog(LOG_FINE) << "Starting " << C_OBJ << "Environment" <<
		C_RESET << " creation .." << logger::endlog(1);
	this->initWindow();
	this->initGlew();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	this->_inputs = new Inputs();

	InputHandler(this->_window);
	logger::sslog(LOG_FINE, -1) << C_OBJ << "Environment" << C_RESET <<
		" created successfully" << logger::endlog();
	logger::sslog(LOG_DEBUG) << *this << logger::endlog();
}

Environment		&Environment::getInstance(void)
{
	static Environment		instance;
	return (instance);
}

// Private member functions

void	Environment::initWindow(void) throw(GLFWInitErrorException,
		WindowCreationErrorException)
{
	const GLFWvidmode	*mode;
	GLint				major;
	GLint				minor;

	logger::sslog(LOG_FINE) << "Starting " << C_OBJ << "Window" << C_RESET
	<< " creation ..." << logger::endlog(1);

	if (!glfwInit())
	{
		throw(GLFWInitErrorException());
	}
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_DECORATED, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 16);
	if (FULLSCREEN)
		createWindow(WIDTH, HEIGHT, "Zatacka", glfwGetPrimaryMonitor(),
				NULL);
	else
		createWindow(WIDTH, HEIGHT, "Zatacka", NULL, NULL);
	if (!this->_window)
		throw(WindowCreationErrorException());
	mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwSetWindowPos(this->_window, (mode->width - WIDTH) / 2,
			(mode->height - HEIGHT) / 2);
	glfwMakeContextCurrent(this->_window);
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);

	logger::sslog(LOG_FINE) << "Using Opengl " << major << "." << minor <<
		logger::endlog();
	logger::sslog(LOG_FINE) << "Gl version: " << glGetString(GL_VERSION) <<
		logger::endlog();
	logger::sslog(LOG_FINE,-1) << C_OBJ << "Window" << C_RESET <<
		" created successfully" << logger::endlog();
}

void	Environment::initGlew(void) throw(GlewInitErrorException)
{
	# if defined(_WIN32) || defined(__CYGWIN__)
		glewExperimental = GL_TRUE;
		GLenum err = glewInit();
		if (err != GLEW_OK)
			throw(GlewInitErrorException((char *)glewGetErrorString(err)));
		logger::sslog(LOG_FINE) << C_OBJ << "Glew" << C_RESET <<
			" initialized." << logger::endlog();
	# endif
}

void	Environment::createWindow(int width, int height, const char *title,
		GLFWmonitor *monitor, GLFWwindow *share)
{
	GLint		major;
	GLint		minor;

	major = 4;
	while (major > 0)
	{
		minor = 9;
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
		while (minor >= 0)
		{
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
			if ((_window = glfwCreateWindow(width, height, title,
							monitor, share)))
				return;
			minor--;
		}
		major--;
	}
}
/*
void	Environment::keyEvent(const int key, const int action)
{
	this->_inputs->keyEvent(key, action);
}*/

// Accessors

GLFWwindow	*Environment::getWindow(void) const
{
	return (this->_window);
}

Inputs		&Environment::getInputs(void) const
{
	return (*this->_inputs);
}


// Print

void		Environment::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Environment" << C_RESET << ": {" << logger::tab(1);
	o << C_VAR << "inputs" << C_RESET << " = " << *this->_inputs;
	o << logger::tab(-1) << "}" ;
}

}

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   CursorMoveControllable.cpp                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 12:44:02 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:36:12 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "CursorMoveControllable.hpp"
#include "Inputs.hpp"

namespace agl
{

// Constroctor - Destructor

CursorMoveControllable::CursorMoveControllable(Inputs &inputs) : _cursor_inputs(inputs)
{
	this->enable();
}

CursorMoveControllable::~CursorMoveControllable(void)
{
	this->disable();
}

// Member Functions

void	CursorMoveControllable::enable(void)
{
	this->_cursor_inputs.addCursorMoveControllable(this);
}

void	CursorMoveControllable::disable(void)
{
	this->_cursor_inputs.removeCursorMoveControllable(this);
}

}

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   FontTexture.cpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-16 18:27:57 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:30:57 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FontTexture.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

FontTexture::FontTexture(const std::string &path, const unsigned int char_width,
		const unsigned int char_height, const unsigned int start, const unsigned int nb)
		throw(InvalidFileException) :
	Texture(path),
	_start(start), _nb(nb),
	_char_width(char_width), _char_height(char_height)
{
	logger::sslog(LOG_DEBUG) << "Creating FontTexture" << logger::endlog();
	this->load_char_length();
	logger::sslog(LOG_DEBUG) << "FontTexture created" << logger::endlog();
}

FontTexture::~FontTexture(void)
{
	delete [] this->_char_length;
	delete [] this->_char_x;
}

// Member Functions

void			FontTexture::load_char_length(void)
{
	unsigned int	i;
	unsigned int	j;
	unsigned int	first;
	unsigned int	last;
	unsigned int	x;
	unsigned int	y;
	unsigned char	*col;
	unsigned int	nb;

	this->_char_length = new unsigned int[this->_nb];
	this->_char_x = new unsigned int[this->_nb];
	nb = 0;
	y = 0;
	while (y < this->_height)
	{
		x = 0;
		while (x < this->_width)
		{
			first = this->_char_width;
			last = this->_char_height;
			i = 0;
			while (i < this->_char_width)
			{
				j = 0;
				while (j < this->_char_height)
				{
					col = this->_data + ((this->_height - y - j - 1) * this->_width * 3 + x * 3 + i * 3);
					if (col[0] != 255 && col[1] != 255 && col[2] != 255)
					{
						if (first == this->_char_width)
							first = i;
						last = i;
					}
					++j;
				}
				++i;
			}
			if (first == this->_char_width)
			{
				this->_char_x[nb] = 0;
				this->_char_length[nb++] = this->_char_width / 2.0f;
			}
			else
			{
				if (last == this->_char_height)
					last = first;
				this->_char_x[nb] = first;
				this->_char_length[nb++] = last - first + 2;
			}
			x += this->_char_width;
		}
		y += this->_char_height;
	}
}

// Accessors

unsigned int	FontTexture::getCharLength(const unsigned char c) const
{
	if (c >= this->_start && c < this->_start + this->_nb)
		return (this->_char_length[c - this->_start]);
	return (0);
}

unsigned int	FontTexture::getCharX(const unsigned char c) const
{
	if (c >= this->_start && c < this->_start + this->_nb)
		return (this->_char_x[c - this->_start]);
	return (0);
}

unsigned int	FontTexture::getCharWidth(void) const
{
	return (this->_char_width);
}

unsigned int	FontTexture::getCharHeight(void) const
{
	return (this->_char_height);
}

unsigned int	FontTexture::getCharId(const unsigned char c) const
{
	return (c - this->_start);
}

// Print

void			FontTexture::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "FontTexture" << C_RESET << ": {" << logger::tab(1);
	Texture::objectPrint(o);
	o << ",\n";
	o << C_VAR << "start" << C_RESET << " = " << this->_start << ",\n";
	o << C_VAR << "nb" << C_RESET << " = " << this->_nb << ",\n";
	o << C_VAR << "char width" << C_RESET << " = " << this->_char_width << ",\n";
	o << C_VAR << "char height" << C_RESET << " = " << this->_char_height << ",\n";
	o << logger::tab(-1) << "}";
}

}

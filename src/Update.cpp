/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Update.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/02 16:15:53 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Update.hpp"
#include "Logger.hpp"

namespace agl
{
// Constructor - Destructor

Update::Update(void)
{

}

Update::~Update(void)
{
	int		i;
	std::forward_list< SmartPointer<Updatable> >::iterator	it;

	i = 0;
	while (i < MAX_UPDATE_LEVEL)
	{
		while (!_updatableObjects[i].empty())
		{
			it = _updatableObjects[i].begin();
			(*it)->kill();
			_updatableObjects[i].pop_front();
		}

		++i;
	}
}

// Member functions

void	Update::addUpdatable(Updatable *updatable, const int level)
{
	if (level < MAX_UPDATE_LEVEL)
		this->_updatableObjects[level].push_front(updatable);
}

void	Update::update(const GameTime &game_time)
{
	Updatable		*updatable;
	int 									i;
	std::forward_list< SmartPointer<Updatable> >::iterator	it;
	std::forward_list< SmartPointer<Updatable> >::iterator	buffer;

	i = 0;
	while (i < MAX_UPDATE_LEVEL)
	{
		if(!this->_updatableObjects[i].empty())
		{
			it = this->_updatableObjects[i].begin();
			buffer = it;
			while (it != this->_updatableObjects[i].end())
			{
				updatable = *it;
				if (updatable->isAlive())
					updatable->update(game_time);
				if (!updatable->isAlive())
				{
					if (it == buffer)
					{
						buffer++;
						this->_updatableObjects[i].pop_front();
						it = buffer;
					}
					else
					{
						this->_updatableObjects[i].erase_after(buffer);
						it = buffer;
						++it;
					}
				}
				else
				{
					buffer = it;
					++it;
				}
			}
		}
		++i;
	}
}

// Print

void	Update::objectPrint(std::ostream &o) const
{
	int					i;
	int					size;
	std::forward_list< SmartPointer<Updatable> >::const_iterator	it;

	o << C_OBJ << "Update" << C_RESET << ": {" << logger::tab(1);
	i = 0;
	while (i < MAX_UPDATE_LEVEL)
	{
		size = distance(this->_updatableObjects[i].begin(), this->_updatableObjects[i].end());
		o << C_VAR << "updatable" << C_RESET << "[" << i << "] (" << size << ")";
		if (size)
		{
			o << ": {" << logger::tab(1);
			it = this->_updatableObjects[i].begin();
			while (it != this->_updatableObjects[i].end())
			{
				o << **it;
				if (++it != this->_updatableObjects[i].end())
					o << ",\n";
			}
			o << logger::tab(-1) << "}";
		}
		if (++i < MAX_UPDATE_LEVEL)
			o << ",\n";
	}
	o << logger::tab(-1) << "}";
}

}

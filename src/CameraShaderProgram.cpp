/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   CameraShaderProgram.cpp                         |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-16 17:09:13 by Bastien             |\________\          */
/*   Updated: 2016/06/16 15:28:53 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "CameraShaderProgram.hpp"
#include "Camera.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

CameraShaderProgram::CameraShaderProgram(const SourceShaderProgram &sources)
	throw(InvalidShaderProgramException) : ShaderProgram(sources)
{
	this->_id_cam_pos = glGetUniformLocation(this->_id, "cam_pos");
	this->_id_cam_size = glGetUniformLocation(this->_id, "cam_size");
}

CameraShaderProgram::CameraShaderProgram(const char *vertex_shader_path,
			const char *geometry_shader_path,
			const char *fragment_shader_path) throw(InvalidShaderProgramException) :
		ShaderProgram(vertex_shader_path, geometry_shader_path, fragment_shader_path)
{
	this->_id_cam_pos = glGetUniformLocation(this->_id, "cam_pos");
	this->_id_cam_size = glGetUniformLocation(this->_id, "cam_size");
}

CameraShaderProgram::~CameraShaderProgram(void)
{

}

// Member Functions

void	CameraShaderProgram::updateCamera(const Camera &camera) const
{
	glUniform2fv(this->_id_cam_pos, 1, camera.getPosition().getXY());
	glUniform2fv(this->_id_cam_size, 1, camera.getSize().getXY());
}

// Print

void	CameraShaderProgram::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "CameraShaderProgram" << C_RESET << ": {" << logger::tab(1);
	ShaderProgram::objectPrint(o);
	o << ",\n";
	o << C_VAR << "id_cam_pos" << C_RESET << " = " << this->_id_cam_pos << ",\n";
	o << C_VAR << "id_cam_size" << C_RESET << " = " << this->_id_cam_size;
	o << logger::tab(-1) << "}";
}

}

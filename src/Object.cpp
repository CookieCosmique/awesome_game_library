/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Object.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 09:35:49 by Bastien             |\________\          */
/*   Updated: 2016/06/01 18:38:45 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Object.hpp"

namespace agl
{
//Destructor
Object::~Object()
{

}

//Operator

std::ostream	&operator<<(std::ostream &o, const Object &rhs)
{
	rhs.objectPrint(o);
	return (o);
}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Texture.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/14 14:54:24 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/02 16:28:01 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Texture.hpp"
#include "Logger.hpp"

namespace agl
{

// Exceptions

const char	*Texture::InvalidFileException::what(void) const throw()
{
	return ("InvalidFileException");
}

// Constructor - Destructor

Texture::Texture(const std::string &path) throw(InvalidFileException) :
	_path(path)
{
	this->loadBMP(this->_path);
	glGenTextures(1, &(this->_id));
	glBindTexture(GL_TEXTURE_2D, this->_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, this->_width, this->_height, 0,
			GL_BGR, GL_UNSIGNED_BYTE, this->_data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

Texture::~Texture(void)
{
	glDeleteTextures(1, &(this->_id));
	delete [] this->_data;
}

// Member Functions

void			Texture::loadBMP(const std::string &path) throw(InvalidFileException)
{
	FILE			*file;
	unsigned char	hd[54];
	unsigned int	data_pos;

	if (!(file = fopen(path.c_str(), "rb")))
		throw (InvalidFileException());
	if (fread(hd, 1, 54, file) != 54 || hd[0] != 'B' || hd[1] != 'M')
		throw (InvalidFileException());
	if ((data_pos = *(int*)&(hd[0x0A])) == 0)
		data_pos = 54;
	this->_width = *(int*)&(hd[0x12]);
	this->_height = *(int*)&(hd[0x16]);
	if ((this->_size = *(int*)&(hd[0x22])) == 0)
		this->_size = this->_width * this->_height * 3;
	this->_data = new unsigned char[this->_size];
	fread(this->_data, 1, this->_size, file);
	fclose(file);
}

void			Texture::bind(void) const
{
	glBindTexture(GL_TEXTURE_2D, this->_id);
}

// Accessors

unsigned int	Texture::getWidth(void) const
{
	return (this->_width);
}

unsigned int	Texture::getHeight(void) const
{
	return (this->_height);
}

// Print

void			Texture::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Texture" << C_RESET << ": {" << logger::tab(1);
	o << C_VAR << "path" << C_RESET << " = " << C_STR << this->_path << C_RESET << ",\n";
	o << C_VAR << "id" << C_RESET << " = " << this->_id << ",\n";
	o << C_VAR << "width" << C_RESET << " = " << this->_width << ",\n";
	o << C_VAR << "height" << C_RESET << " = " << this->_height << ",\n";
	o << C_VAR << "size" << C_RESET << " = " << this->_size;
	o << logger::tab(-1) << "}";
}

}

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ShaderProgram.cpp                               |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-19 17:29:14 by Bastien             |\________\          */
/*   Updated: 2016/06/16 17:34:26 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cstring>
#include "Logger.hpp"
#include "ShaderProgram.hpp"
#include "SourceShaderProgram.hpp"

namespace agl
{

// Exception

const char		*ShaderProgram::InvalidShaderProgramException::what(void) const throw()
{
	return ("InvalidShaderProgramException");
}

// Constructor - Destructor

ShaderProgram::ShaderProgram(const SourceShaderProgram &sources)
	throw(InvalidShaderProgramException) :
		_vertexShaderPath(sources.getVertex()),
		_geometryShaderPath(sources.getGeometry()),
		_fragmentShaderPath(sources.getFragment())
{
	this->_idVertexShader = this->loadShader(this->_vertexShaderPath,
			GL_VERTEX_SHADER);
	this->_idGeometryShader = this->loadShader(this->_geometryShaderPath,
			GL_GEOMETRY_SHADER);
	this->_idFragmentShader = this->loadShader(this->_fragmentShaderPath,
			GL_FRAGMENT_SHADER);
	if (!this->_idVertexShader || !this->_idGeometryShader ||
			!this->_idFragmentShader)
	{
		logger::sslog(LOG_ERROR, -1) << "Error while loading shaders." <<
			logger::endlog();
		throw(InvalidShaderProgramException());
	}
	this->_id = glCreateProgram();
	glAttachShader(this->_id, this->_idVertexShader);
	glAttachShader(this->_id, this->_idGeometryShader);
	glAttachShader(this->_id, this->_idFragmentShader);
	glBindFragDataLocation(this->_id, 0, "out_color");
	glLinkProgram(this->_id);
	this->use();

}

ShaderProgram::ShaderProgram(const char *vertex_shader_path,
							 const char *geometry_shader_path,
							 const char *fragment_shader_path)
			throw(InvalidShaderProgramException) :
		_vertexShaderPath(vertex_shader_path),
		_geometryShaderPath(geometry_shader_path),
		_fragmentShaderPath(fragment_shader_path)
{
	this->_idVertexShader = this->loadShader(this->load_file(vertex_shader_path),
			GL_VERTEX_SHADER);
	this->_idGeometryShader = this->loadShader(this->load_file(geometry_shader_path),
			GL_GEOMETRY_SHADER);
	this->_idFragmentShader = this->loadShader(this->load_file(fragment_shader_path),
			GL_FRAGMENT_SHADER);
	if (!this->_idVertexShader || !this->_idGeometryShader ||
			!this->_idFragmentShader)
	{
		logger::sslog(LOG_ERROR, -1) << "Error while loading shaders." <<
			logger::endlog();
		throw(InvalidShaderProgramException());
	}
	this->_id = glCreateProgram();
	glAttachShader(this->_id, this->_idVertexShader);
	glAttachShader(this->_id, this->_idGeometryShader);
	glAttachShader(this->_id, this->_idFragmentShader);
	glBindFragDataLocation(this->_id, 0, "out_color");
	glLinkProgram(this->_id);
	this->use();
}

ShaderProgram::~ShaderProgram(void)
{
	glDetachShader(this->_id, this->_idVertexShader);
	glDeleteShader(this->_idVertexShader);
	glDetachShader(this->_id, this->_idGeometryShader);
	glDeleteShader(this->_idGeometryShader);
	glDetachShader(this->_id, this->_idFragmentShader);
	glDeleteShader(this->_idFragmentShader);
	glDeleteProgram(this->_id);
}

// Private functions

GLchar			*ShaderProgram::load_file(const char *path) const
{
	GLchar					*source;
	std::ifstream			file(path, std::ifstream::in);
	std::string				line;
	std::stringstream		ss;

	if (!file.is_open())
	{
		logger::sslog(LOG_ERROR, -1) << "Can't open file " << C_STR <<
			"\"" << path << "\"" << C_RESET << logger::endlog();
		return (NULL);
	}
	while (std::getline(file, line))
	{
		ss << line << "\n";
	}
	source = new char[ss.str().length() + 1];
	std::strcpy(source, ss.str().c_str());
	file.close();
	return (source);
}

int				ShaderProgram::compileShader(const GLchar *source,
		GLenum shader_type) const
{
	GLuint					id_shader;
	GLint					status;
	char					buffer[512];

	if (!source)
		return (0);
	if (!(id_shader = glCreateShader(shader_type)))
	{
		logger::sslog(LOG_ERROR, -1) << "Can't create shader." << logger::endlog();
		return (0);
	}
	glShaderSource(id_shader, 1, &source, NULL);
	glCompileShader(id_shader);
	glGetShaderiv(id_shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderInfoLog(id_shader, 512, NULL, buffer);
		logger::sslog(LOG_ERROR, -1) << "Shader error: " << buffer <<
			logger::endlog();
		return (0);
	}
	return (id_shader);
}

int				ShaderProgram::loadShader(GLchar *source,
		GLenum shader_type) const
{
	GLuint			id_shader;

	id_shader = this->compileShader(source, shader_type);
	delete [] source;
	return (id_shader);
}

int				ShaderProgram::loadShader(const std::string &source,
		GLenum shader_type) const
{
	return (this->compileShader(source.c_str(), shader_type));
}

// Public functions

void			ShaderProgram::use(void) const
{
	glUseProgram(this->_id);
}

// Accessors

GLuint	ShaderProgram::getID(void) const
{
	return (this->_id);
}

// Print

void	ShaderProgram::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "ShaderProgram" << C_RESET << ": {" << logger::tab(1);
	o << C_VAR << "id" << C_RESET << " = " << _id << ",\n";
	o << C_VAR << "idVertexShader" << C_RESET << " = " << _idVertexShader;
	o << C_VAR << ", vertexShaderPath" << C_RESET << " = " << C_STR << "\"" << _vertexShaderPath << "\"" << C_RESET << ",\n";
	o << C_VAR << "idGeometryShader" << C_RESET << " = " << _idGeometryShader;
	o << C_VAR << ", geometryShaderPath" << C_RESET << " = " << C_STR << "\"" << _geometryShaderPath << "\"" << C_RESET << ",\n";
	o << C_VAR << "idFragmentShader" << C_RESET << " = " << _idFragmentShader;
	o << C_VAR << ", fragmentShaderPath" << C_RESET << " = " << C_STR << "\"" << _fragmentShaderPath << "\"" << C_RESET;
	o << logger::tab(-1) << "}";
}

}

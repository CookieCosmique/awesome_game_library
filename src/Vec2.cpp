/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Vec2.cpp                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 18:38:25 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cmath>
#include "Vec2.hpp"
#include "Logger.hpp"

namespace agl
{
//Constructor and Desctructor

Vec2::Vec2(void) : x(_xy[0]), y(_xy[1])
{
	_xy[0] = 0;
	_xy[1] = 0;
}

Vec2::Vec2(float x, float y) : x(_xy[0]), y(_xy[1])
{
	_xy[0] = x;
	_xy[1] = y;
}

Vec2::Vec2(float *xy) : x(_xy[0]), y(_xy[1])
{
	_xy[0] = xy[0];
	_xy[1] = xy[1];
}

Vec2::Vec2(const Vec2 &other) : x(_xy[0]), y(_xy[1])
{
	_xy[0] = other.x;
	_xy[1] = other.y;
}

// Getters and Setters

const float	*Vec2::getXY(void) const
{
	return (_xy);
}

float	Vec2::length(void) const
{
	return sqrt(_xy[0] * _xy[0] + _xy[1] * _xy[1]);
}

Vec2	Vec2::vx(void) const
{
	return Vec2(_xy[0], 0.0f);
}

Vec2	Vec2::vy(void) const
{
	return Vec2(0.0f, _xy[1]);
}

bool	Vec2::isNull(void) const
{
	return (_xy[0] == 0.0f && _xy[1] == 0.0f);
}

void	Vec2::setX(float x)
{
	_xy[0] = x;
}

void	Vec2::setY(float y)
{
	_xy[1] = y;
}

// On vec operation ( will alter the vector )

Vec2	&Vec2::normalize(void)
{
	Vec2	tmp = this->tangent();

	_xy[0] = tmp._xy[0];
	_xy[1] = tmp._xy[1];

	return (*this);
}

Vec2	&Vec2::shift(void)
{
	float	tmp;

	tmp = _xy[0];
	_xy[0] = -_xy[1];
	_xy[1] = tmp;

	return (*this);
}

Vec2	&Vec2::shiftX(void)
{
	float	tmp;

	tmp = _xy[0];
	_xy[0] = _xy[1];
	_xy[1] = -tmp;

	return (*this);
}

Vec2	&Vec2::rotate(const float theta)
{
	Vec2	tmp(rotated(theta));
	
	_xy[0] = tmp._xy[0];
	_xy[1] = tmp._xy[1];

	return (*this);
}

// Get a transformation of the vector, without modifying it

Vec2	Vec2::tangent(void) const
{
	float	tmp;

	tmp = length();
	if(tmp != 0)
		return Vec2(_xy[0]/tmp, _xy[1]/tmp);
	else
		return Vec2(0.0,0.0);
}

Vec2	Vec2::normalized(void) const
{
	return tangent();
}

Vec2	Vec2::normal(void) const
{
	float	tmp;

	tmp = length();
	if(tmp != 0)
		return Vec2(-_xy[1]/tmp, _xy[0]/tmp);
	else
		return Vec2(0.0,0.0);
}

Vec2	Vec2::rotated(const float theta) const
{
	return Vec2(_xy[0] * cos(theta) - _xy[1] * sin(theta), _xy[0] * sin(theta) + _xy[1] * cos(theta));
}

// Operator 
Vec2	Vec2::operator+(const Vec2 &other) const
{
	return Vec2(_xy[0] + other._xy[0], _xy[1] + other._xy[1]);
}

Vec2	Vec2::operator-(const Vec2 &other) const
{
	return Vec2(_xy[0] - other._xy[0], _xy[1] - other._xy[1]);
}

Vec2	Vec2::operator-(void) const
{
	return Vec2(- _xy[0],- _xy[1]);
}

Vec2	Vec2::operator/(const Vec2 &other) const
{
	return (Vec2(this->x / other.x, this->y / other.y));
}

Vec2	Vec2::operator/(const float coef) const
{
	return (Vec2(this->x / coef, this->y / coef));
}

float	Vec2::operator*(const Vec2 &other) const
{
	return _xy[0] * other._xy[0] + _xy[1] * other._xy[1];
}

float	Vec2::operator^(const Vec2 &other) const
{
	return _xy[0] * other._xy[1] - _xy[1] * other._xy[0];
}

Vec2	Vec2::operator%(const Vec2 &other) const
{
	return Vec2( vec2::moded(_xy[0], other._xy[0]), vec2::moded(_xy[1], other._xy[1]));
}

Vec2	&Vec2::operator=(const Vec2 &other)
{
	_xy[0] = other._xy[0];
	_xy[1] = other._xy[1];

	return (*this);
}

Vec2	&Vec2::operator+=(const Vec2 &other)
{
	Vec2	tmp(*this + other);

	_xy[0] = tmp._xy[0];
	_xy[1] = tmp._xy[1];

	return (*this);
}

Vec2	&Vec2::operator-=(const Vec2 &other)
{
	Vec2	tmp(*this - other);

	_xy[0] = tmp._xy[0];
	_xy[1] = tmp._xy[1];

	return (*this);
}

Vec2	&Vec2::operator*=(const float scalar)
{
	Vec2	tmp(*this * scalar);

	_xy[0] = tmp._xy[0];
	_xy[1] = tmp._xy[1];

	return (*this);
}

Vec2	operator*(const float scalar, const Vec2 &vec)
{
	return Vec2(vec.x * scalar, vec.y * scalar);
}

Vec2	operator*(const Vec2 &vec, const float scalar)
{
	return (Vec2(vec.x * scalar, vec.y * scalar));
}

Vec2	operator*(const double scalar, const Vec2 &vec)
{
	return Vec2(vec.x * scalar, vec.y * scalar);
}

Vec2	operator*(const Vec2 &vec, const double scalar)
{
	return (Vec2(vec.x * scalar, vec.y * scalar));
}

Vec2	operator*(const int scalar, const Vec2 &vec)
{
	return Vec2(vec.x * scalar, vec.y * scalar);
}

Vec2	operator*(const Vec2 &vec, const int scalar)
{
	return (Vec2(vec.x * scalar, vec.y * scalar));
}

bool	Vec2::operator==(const Vec2 &other) const
{
	return (this->x == other.x && this->y == other.y);
}

// Extern operator (will only call the object function)

Vec2	tangent(const Vec2 &vec)
{
	return (vec.tangent());
}

Vec2	normal(const Vec2 &vec)
{
	return (vec.normal());
}

Vec2	rotated(const Vec2 &vec, const float theta)
{
	return (vec.rotated(theta));
}

// << operator

std::ostream	&operator<<(std::ostream &o, const Vec2 &vec)
{
	o << C_OBJ << "Vec2" << C_RESET << "(" << vec.x << ", " << vec.y << ")";
	return (o);
}

// Vec2 namespace

namespace vec2
{
	Vec2	up(0.0, 1.0);
	Vec2	down(0.0, -1.0);
	Vec2	right(1.0, 0.0);
	Vec2	left(-1.0, 0.0);
	Vec2	null(0.0, 0.0);

	Vec2	x(1.0, 0.0);
	Vec2	y(0.0, 1.0);

	float	moded(float a, float b)
	{
		return floor(a / b) * b;
	}

	float	moduled(float a, float b)
	{
		return a - (floor(a / b) * b);
	}

	int		floor(float a)
	{
		float	decPart;

		if (a > 0)
			return int(a);
		else
		{
			decPart = a - int(a);
			if(decPart == 0)
				return int(a);
			else
				return int(a) - 1;
		}
	}
}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Camera.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/07 11:40:14 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/01 18:35:15 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Camera.hpp"
#include "Logger.hpp"

namespace agl
{
// Constructor - Destructor

Camera::Camera(const Vec2 &position, const Vec2 &size) :
	Spawnable(),
	_position(position), _size(size)
{
}

Camera::~Camera(void)
{
}

// Accessors

const Vec2		&Camera::getPosition(void) const
{
	return (this->_position);
}

const Vec2		&Camera::getSize(void) const
{
	return (this->_size);
}

// Print

void			Camera::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Camera" << C_RESET << ": { " << logger::tab(1);
	o << C_VAR << "position" << C_RESET << " = " << this->_position;
	o << ", ";
	o << C_VAR << "size" << C_RESET << " = " << this->_size;
	o << logger::tab(-1) << " }";
}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TexturedComponentVAO.cpp                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-14 17:25:22 by Bastien             |\________\          */
/*   Updated: 2016/06/16 15:16:37 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "TexturedComponentVAO.hpp"
#include "Texture.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

TexturedComponentVAO::TexturedComponentVAO(const GLuint program_id,
		const Vec2 &position, const Vec2 &size, const std::string &texture_path)
	throw(Texture::InvalidFileException) : SquareVAO(program_id, position, size)
{
	GLfloat		uvs[8] = {
		 0.0f, 0.0f,
		 0.0f, 1.0f,
		 1.0f, 0.0f,
		 1.0f, 1.0f
	};

	this->_texture = new Texture(texture_path);
	this->_vbo_uv = loadVBO(program_id, sizeof(GLfloat) * 8, uvs, GL_FLOAT, 2,
			"UVs", 2, GL_STATIC_DRAW);
}

TexturedComponentVAO::~TexturedComponentVAO(void)
{
	delete this->_texture;
}

// Member Functions

void		TexturedComponentVAO::render(void) const
{
	this->bind();
	this->_texture->bind();
	SquareVAO::render();
}

// Print

void		TexturedComponentVAO::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "TexturedComponentVAO" << C_RESET << ": {" << logger::tab(1);
	SquareVAO::objectPrint(o);
	o << ",\n";
	o << C_VAR << "vbo uv" << C_RESET << " = " << this->_vbo_uv << ",\n";
	o << C_VAR << "texture" << C_RESET << " = " << this->_texture;
	o << logger::tab(-1) << "}";
}

}

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Spawnable.cpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 14:02:35 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 18:38:07 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Spawnable.hpp"
#include "Logger.hpp"

namespace agl
{
// Constructor - Destructor

Spawnable::Spawnable(void)
{
	this->_toKill = false;
	this->_references = 0;
}

Spawnable::~Spawnable(void)
{

}

// Member function

bool	Spawnable::kill(void)
{
	this->_toKill = true;
	return (this->isKillable());
}

bool	Spawnable::isAlive(void) const
{
	return (!this->_toKill);
}

bool	Spawnable::isKillable(void) const
{
	return (this->_references <= 0);
}

// Operator overload

Spawnable &Spawnable::operator++(int)
{
	this->_references++;
	return (*this);
}

Spawnable &Spawnable::operator--(int)
{
	this->_references--;
	if (this->isKillable() && !this->isAlive())
		delete (this);
	return (*this);
}
/*
Spawnable &operator--(Spawnable &spawn, int)
{
	spawn._references--;
	if (spawn.isKillable() && !spawn.isAlive())
		delete (&spawn);
	return (spawn);
}
*/
// Print

void	Spawnable::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Spawnable" << C_RESET << ": { ";
	o << C_VAR << "references" << C_RESET << " = " << this->_references << ", ";
	o << C_VAR << "toKill" << C_RESET << " = " << this->_toKill;
	o << " }";
}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   SceneComponent.cpp                              |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 15:33:34 by Bastien             |\________\          */
/*   Updated: 2016/06/14 15:01:57 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SceneComponent.hpp"
#include "Area.hpp"
#include "Camera.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

SceneComponent::SceneComponent(Scene &scene, Area *area,
		const Vec2 &position, const Vec2 &size,
		const PositionType pos_type, const PositionType size_type) :
	_area(area), _scene(scene)
{
	this->initSize(size, size_type);
	this->initPosition(position, pos_type);
}

SceneComponent::~SceneComponent(void)
{

}

// Member Functions

void			SceneComponent::scale(const float x, const float y)
{
	this->_position.x -= (this->_size.x * x - this->_size.x) / 2.0f;
	this->_position.y -= (this->_size.y * y - this->_size.y) / 2.0f;
	this->_size.x *= x;
	this->_size.y *= y;
	this->onModif();
}

void			SceneComponent::scale(const Vec2 &coef)
{
	this->scale(coef.x, coef.y);
}

void			SceneComponent::scale(const float coef)
{
	this->scale(coef, coef);
}

void			SceneComponent::scaleArea(const float x, const float y)
{
	Vec2		ratio;
	Vec2		new_pos;

	if (this->_area != NULL)
	{
		ratio = (this->_position - this->_area->getPosition()) /
			this->_area->getSize();
		this->scale(x, y);
		new_pos = (this->_area->getPosition() -
				(Vec2(this->_area->getSize().x * x, this->_area->getSize().y * y)
				 - this->_area->getSize()) / 2.0f) +
			Vec2(this->_area->getSize().x * ratio.x * x,
					this->_area->getSize().y * ratio.y * y);
		this->translate(new_pos - this->_position);
	}
	else
		this->scale(x, y);
}

void			SceneComponent::scaleArea(const Vec2 &coef)
{
	this->scaleArea(coef.x, coef.y);
}

void			SceneComponent::scaleArea(const float coef)
{
	this->scaleArea(coef, coef);
}

void			SceneComponent::translate(const float x, const float y)
{
	this->_position.x += x;
	this->_position.y += y;
	this->onModif();
}

void			SceneComponent::translate(const Vec2 &vec)
{
	this->translate(vec.x, vec.y);
}

Vec2			SceneComponent::getRatio(const Vec2 &ratio) const
{
	ratio.x = std::max(ratio.x, 0.0f);
	ratio.y = std::max(ratio.y, 0.0f);
	ratio.x = std::min(ratio.x, 1.0f);
	ratio.y = std::min(ratio.y, 1.0f);
	return (ratio);
}

void			SceneComponent::initPositionRatio(const Vec2 &position,
		const Vec2 &size, const Vec2 &ratio)
{
	float		start;
	float		end;

	start = position.x + this->_size.x / 2.0f;
	end = position.x + size.x - this->_size.x / 2.0f;
	this->_position.x = start + (end - start) * ratio.x - this->_size.x / 2.0f;

	start = position.y + this->_size.y / 2.0f;
	end = position.y + size.y - this->_size.y / 2.0f;
	this->_position.y = start + (end - start) * ratio.y - this->_size.y / 2.0f;
}

void			SceneComponent::setPositionRatio(const Vec2 &position,
		const Vec2 &size, const Vec2 &ratio)
{
	Vec2		new_position;
	float		start;
	float		end;

	start = position.x + this->_size.x / 2.0f;
	end = position.x + size.x - this->_size.x / 2.0f;
	new_position.x = start + (end - start) * ratio.x - this->_size.x / 2.0f;

	start = position.y + this->_size.y / 2.0f;
	end = position.y + size.y - this->_size.y / 2.0f;
	new_position.y = start + (end - start) * ratio.y - this->_size.y / 2.0f;

	this->translate(new_position - this->_position);
}

void			SceneComponent::initSize(const Vec2 &size,
		const PositionType type)
{
	if (type == PositionType::POS_NORMAL)
		this->_size = size;
	else if (type == PositionType::POS_RATIO)
	{
		Vec2 ratio = getRatio(size);
		if (this->_area)
		{
			this->_size.x = this->_area->getSize().x * ratio.x;
			this->_size.y = this->_area->getSize().y * ratio.y;
		}
		else
		{
			this->_size.x = this->_scene.getCamera().getSize().x * ratio.x;
			this->_size.y = this->_scene.getCamera().getSize().y * ratio.y;
		}
	}
}

void			SceneComponent::setSize(const Vec2 &size,
		const PositionType type)
{
	Vec2		new_size;

	if (type == PositionType::POS_NORMAL)
		new_size = size;
	else if (type == PositionType::POS_RATIO)
	{
		Vec2 ratio = getRatio(size);
		if (this->_area)
		{
			new_size.x = this->_area->getSize().x * ratio.x;
			new_size.y = this->_area->getSize().y * ratio.y;
		}
		else
		{
			new_size.x = this->_scene.getCamera().getSize().x * ratio.x;
			new_size.y = this->_scene.getCamera().getSize().y * ratio.y;
		}
	}
	this->scale(new_size / this->_size);
}

void			SceneComponent::initPosition(const Vec2 &position,
		const PositionType type)
{
	if (type == PositionType::POS_NORMAL)
	{
		if (this->_area)
			this->_position = position + this->_area->getPosition();
		else
			this->_position = position;
	}
	else if (type == PositionType::POS_RATIO)
	{
		Vec2 ratio = getRatio(position);
		if (this->_area)
			this->initPositionRatio(this->_area->getPosition(),
					this->_area->getSize(), ratio);
		else
			this->initPositionRatio(this->_scene.getCamera().getPosition(),
					this->_scene.getCamera().getSize(), ratio);
	}
}

void			SceneComponent::setPosition(const Vec2 &position,
		const PositionType type)
{
	Vec2		new_position;

	if (type == PositionType::POS_NORMAL)
	{
		if (this->_area)
			new_position = position + this->_area->getPosition();
		else
			new_position = position;
		this->translate(new_position - this->_position);
	}
	else if (type == PositionType::POS_RATIO)
	{
		Vec2 ratio = getRatio(position);
		if (this->_area)
			this->setPositionRatio(this->_area->getPosition(),
					this->_area->getSize(), ratio);
		else
			this->setPositionRatio(this->_scene.getCamera().getPosition(),
					this->_scene.getCamera().getSize(), ratio);
	}
}

// Accessors

const Vec2		&SceneComponent::getPosition(void) const
{
	return (this->_position);
}

const Vec2		&SceneComponent::getSize(void) const
{
	return (this->_size);
}

// Print

void			SceneComponent::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "SceneComponent" << C_RESET << ": {" << logger::tab(1);
	Spawnable::objectPrint(o);
	o << ",\n";
	o << C_VAR << "position" << C_RESET << " = " << this->_position << ",\n";
	o << C_VAR << "size" << C_RESET << " = " << this->_size;
	o << logger::tab(-1) << "}";
}

}

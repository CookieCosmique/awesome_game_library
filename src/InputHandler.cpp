/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   InputHandler.cpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-01 02:42:14 by Bastien             |\________\          */
/*   Updated: 2016/06/04 16:20:15 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InputHandler.hpp"
#include "Environment.hpp"

namespace agl
{

// Constructor - Destructor

InputHandler::InputHandler(GLFWwindow *window)
{
	glfwSetKeyCallback(window, this->keyCallback);
	glfwSetMouseButtonCallback(window, this->mouseButtonCallback);
	glfwSetCursorPosCallback(window, this->cursorPositionCallback);
}

InputHandler::~InputHandler(void)
{

}

// Member functions

void		InputHandler::keyCallback(GLFWwindow*, int key, int, int action, int)
{
	Environment::getInstance().getInputs().keyEvent(key, action);
}

void		InputHandler::mouseButtonCallback(GLFWwindow*, int button,
		int action, int)
{
	Environment::getInstance().getInputs().mouseButtonEvent(button, action);
}

void		InputHandler::cursorPositionCallback(GLFWwindow*, double xpos,
		double ypos)
{
	Environment::getInstance().getInputs().cursorMoveEvent(xpos / (double)WIDTH,
			ypos / (double)HEIGHT);
}

}

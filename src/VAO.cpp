/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   VAO.cpp                                         |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-31 06:36:50 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:03:36 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "VAO.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

VAO::VAO(void)
{
	glGenVertexArrays(1, &this->_vao);
}

VAO::~VAO(void)
{
	glDeleteVertexArrays(1, &this->_vao);
}

// Member functions

void		VAO::bind(void) const
{
	glBindVertexArray(this->_vao);
}

GLuint		VAO::createVBO(const GLuint program_id, GLenum type,
		const GLuint index, const char *name, GLint nb) const
{
	GLuint vbo;

	this->bind();
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBindAttribLocation(program_id, index, name);
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, nb, type, GL_FALSE, 0, NULL);
	return (vbo);
}

GLuint		VAO::createEBO(void) const
{
	GLuint	ebo;

	this->bind();
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	return (ebo);
}

GLuint		VAO::loadVBO(const GLuint program_id, GLsizeiptr size,
		const GLvoid *data, GLenum type, const GLuint index, const char *name,
		GLint nb, GLenum usage) const
{
	GLuint	vbo;

	vbo = this->createVBO(program_id, type, index, name, nb);
	glBufferData(GL_ARRAY_BUFFER, size, data, usage);
	return (vbo);
}

GLuint		VAO::loadEBO(GLsizeiptr size, const GLvoid *data, GLenum usage) const
{
	GLuint	ebo;

	ebo = this->createEBO();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, usage);
	return (ebo);
}

void		VAO::updateBufferData(GLenum target, GLuint buffer, GLsizeiptr size,
		const GLvoid *data) const
{
	this->bind();
	glBindBuffer(target, buffer);
	glBufferData(target, size, data, GL_DYNAMIC_DRAW);
}

// Print

void		VAO::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "VAO" << C_RESET << ": { ";
	o << C_VAR << "vao" << C_RESET << " = " << this->_vao;
	o << " }";
}

}

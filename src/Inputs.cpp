/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Inputs.cpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-19 15:13:53 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:39:49 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <iomanip>
#include "OpenGL.h"
#include "Inputs.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

Inputs::Inputs(void)
{
	linkMouseButtonAction(GLFW_MOUSE_BUTTON_LEFT, GLFW_PRESS, "LButtonPress");
	linkMouseButtonAction(GLFW_MOUSE_BUTTON_LEFT, GLFW_RELEASE, "LButtonRelease");
	logger::sslog(LOG_FINE) << C_OBJ << "Inputs" << C_RESET << " created." <<
		logger::endlog();
}

Inputs::~Inputs(void)
{
	unlinkMouseButtonAction(GLFW_MOUSE_BUTTON_LEFT, GLFW_PRESS);
	unlinkMouseButtonAction(GLFW_MOUSE_BUTTON_LEFT, GLFW_RELEASE);
	logger::sslog(LOG_FINE) << C_OBJ << "Inputs" << C_RESET << " deleted." <<
		logger::endlog();
}

// Member functions

void		Inputs::linkMouseButtonAction(const int button, const int action,
		const std::string &name)
{
	std::vector<std::string>	&vec = this->_mouseButtonActions[button];

	if (vec.size() < 2)
		vec.resize(2);
	if (!(vec[action].empty()))
		logger::sslog(LOG_WARNING) << "Button " << button << ":" << action <<
			" already used for " << vec[action] << logger::endlog();
	vec[action] = name;
}

void		Inputs::unlinkMouseButtonAction(const int button, const int action)
{
	std::vector<std::string>	&vec = this->_mouseButtonActions[button];

	if (vec.size() < 2)
		vec.resize(2);
	vec[action].clear();
}

void		Inputs::linkKeyAction(const int key, const int action,
		const std::string &name)
{
	std::vector<std::string>	&vec = this->_keyActions[key];

	if (vec.size() < 2)
		vec.resize(2);
	if (!(vec[action].empty()))
		logger::sslog(LOG_WARNING) << "Key " << key << ":" << action <<
			" already used for " << vec[action] << logger::endlog();
	vec[action] = name;
}

void		Inputs::unlinkKeyAction(const int key, const int action)
{
	std::vector<std::string>	&vec = this->_keyActions[key];

	if (vec.size() < 2)
		vec.resize(2);
	vec[action].clear();
}

void		Inputs::addControllable(const std::string &action,
		Controllable *controllable)
{
	this->_controllables[action].push_front(controllable);
}

void		Inputs::removeControllable(const std::string &action,
		Controllable *controllable)
{
	this->_controllables[action].remove(controllable);
}

void		Inputs::removeControllables(const std::string &action)
{
	this->_controllables[action].clear();
}

void		Inputs::addCursorMoveControllable(CursorMoveControllable *controllable)
{
	this->_cursorMoveControllables.push_back(controllable);
}

void		Inputs::removeCursorMoveControllable(CursorMoveControllable *controllable)
{
	this->_cursorMoveControllables.remove(controllable);
}

void		Inputs::registerKeyAction(const int key, const int action,
		const std::string &name, Controllable *controllable)
{
	linkKeyAction(key, action, name);
	addControllable(name, controllable);
}

void		Inputs::unregisterKeyAction(const int key, const int action,
		const std::string &name, Controllable *controllable)
{
	unlinkKeyAction(key, action);
	removeControllable(name, controllable);
}

void		Inputs::keyEvent(const int key, const int action)
{
	std::vector<std::string>			&vec = this->_keyActions[key];
	std::string							str;
	std::list<Controllable*>::iterator	it;

	if (vec.size() <= (unsigned)action)
		return ;
	str = vec[action];
	if (str.empty())
		return ;
	if (!(this->_controllables[str].empty()))
	{
		it = this->_controllables[str].begin();
		while (it != this->_controllables[str].end())
		{
			(*it)->input(str);
			++it;
		}
	}
}

void		Inputs::mouseButtonEvent(const int button, const int action)
{
	std::vector<std::string>			&vec = this->_mouseButtonActions[button];
	std::string							str;
	std::list<Controllable*>::iterator	it;

	if (vec.size() <= (unsigned)action)
		return ;
	str = vec[action];
	if (str.empty())
		return ;
	if (!(this->_controllables[str].empty()))
	{
		it = this->_controllables[str].begin();
		while (it != this->_controllables[str].end())
		{
			(*it)->input(str);
			++it;
		}
	}
}

void		Inputs::cursorMoveEvent(const double xpos, const double ypos)
{
	this->_cursorPosX = xpos;
	this->_cursorPosY = 1.0 - ypos;
	if (!this->_cursorMoveControllables.empty())
	{
		std::list<CursorMoveControllable*>::iterator	it = this->_cursorMoveControllables.begin();
		while (it != this->_cursorMoveControllables.end())
		{
			(*it)->cursorMove(this->_cursorPosX, this->_cursorPosY);
			++it;
		}
	}
}

const std::string	Inputs::getKeyActionName(const int key,
		const int action) const
{
	try
	{
		const std::vector<std::string>		&vec = this->_keyActions.at(key);

		if (vec.size() < (unsigned)action)
			return (std::string(""));
		return (vec[action]);
	}
	catch (std::out_of_range &oor)
	{
		return (std::string(""));
	}
}

const std::string	Inputs::getMouseButtonActionName(const int button,
		const int action) const
{
	try
	{
		const std::vector<std::string>		&vec = this->_mouseButtonActions.at(button);

		if (vec.size() < (unsigned)action)
			return (std::string(""));
		return (vec[action]);
	}
	catch (std::out_of_range &oor)
	{
		return (std::string(""));
	}
}

void				Inputs::getCursorPos(double *x, double *y) const
{
	*x = this->_cursorPosX;
	*y = this->_cursorPosY;
}

// Print

void		Inputs::objectPrint(std::ostream &o) const
{
	std::map<int, std::vector<std::string> >::const_iterator	it = this->_keyActions.begin();

	o << C_OBJ << "Inputs" << C_RESET << ": {" << logger::tab(1);
	o << C_VAR << "actions" << C_RESET << " = {" << logger::tab(1);
	while (it != this->_keyActions.end())
	{
		if (it->second.size() != 0)
		{
			o << C_VAR << it->first << C_RESET << ": { ";
			o << "press: " << C_STR << std::setw(20) << std::setfill(' ') << std::left << it->second[GLFW_PRESS] << C_RESET;
			o << ", ";
			o << "release: " << C_STR << std::setw(20) << std::setfill(' ') << std::left << it->second[GLFW_RELEASE] << C_RESET;
			o << " }";
			if (++it != this->_keyActions.end())
				o << ",\n";
		}
		else
			++it;
	}
	o << logger::tab(-1) << "}"; 
	o << logger::tab(-1) << "}";
}

}

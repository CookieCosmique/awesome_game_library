/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Controllable.cpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-19 16:25:44 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:34:52 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Controllable.hpp"
#include "Inputs.hpp"

namespace agl
{

Controllable::Controllable(Inputs &inputs) :
	_inputs(inputs)
{

}

Controllable::~Controllable(void)
{

}

}

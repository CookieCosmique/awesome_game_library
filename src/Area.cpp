/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Area.cpp                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 16:11:56 by Bastien             |\________\          */
/*   Updated: 2016/06/14 14:36:19 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Area.hpp"
#include "Logger.hpp"

namespace agl
{

// Constructor - Destructor

Area::Area(Scene &scene, Area *area, const Vec2 &position, const Vec2 &size,
		const PositionType pos_type, const PositionType size_type) :
	SceneComponent(scene, area, position, size, pos_type, size_type)
{

}

Area::~Area(void)
{
	std::forward_list< SmartPointer<SceneComponent> >::iterator		it;

	while (!this->_componentList.empty())
	{
		it = this->_componentList.begin();
		(*it)->kill();
		this->_componentList.pop_front();
	}
}

// Member Functions

void			Area::addComponent(SceneComponent *component)
{
	this->_componentList.push_front(component);
}

void			Area::scale(const float x, const float y)
{
	std::forward_list< SmartPointer<SceneComponent> >::iterator		it;

	it = this->_componentList.begin();
	while (it != this->_componentList.end())
	{
		(*it)->scaleArea(x, y);
		++it;
	}
	SceneComponent::scale(x, y);
}

void			Area::translate(const float x, const float y)
{
	std::forward_list< SmartPointer<SceneComponent> >::iterator		it;

	it = this->_componentList.begin();
	while (it != this->_componentList.end())
	{
		(*it)->translate(x, y);
		++it;
	}
	SceneComponent::translate(x, y);
}

// Print

void			Area::objectPrint(std::ostream &o) const
{
	std::forward_list< SmartPointer<SceneComponent> >::const_iterator		it;
	
	o << C_OBJ << "Area" << C_RESET << ": {" << logger::tab(1);
	SceneComponent::objectPrint(o);
	o << ",\n";
	o << C_VAR << "component list" << C_RESET << " = {" << logger::tab(1);
	it = this->_componentList.begin();
	while (it != this->_componentList.end())
	{
		o << **it;
		if (++it != this->_componentList.end())
			o << ",\n";
	}
	o << logger::tab(-1) << "}";
	o << logger::tab(-1) << "}";
}

}

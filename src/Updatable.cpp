/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Updatable.cpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 18:36:19 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Updatable.hpp"
#include "Update.hpp"
#include "Logger.hpp"

namespace agl
{
// Constructor - Destructor

Updatable::Updatable(Update &update, const int level)
{
	update.addUpdatable(this, level);
}

Updatable::~Updatable(void)
{

}

// Print

void		Updatable::objectPrint(std::ostream &o) const
{
	o << C_OBJ << "Updatable" << C_RESET << ": ";
	Spawnable::objectPrint(o);
}
}

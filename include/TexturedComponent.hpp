/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TexturedComponent.hpp                           |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 22:02:55 by Bastien             |\________\          */
/*   Updated: 2016/06/16 17:17:20 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXTURED_COMPONENT_HPP
# define TEXTURED_COMPONENT_HPP

# include "Renderable.hpp"
# include "SceneComponent.hpp"
# include "TexturedComponentShaderProgram.hpp"
# include "Texture.hpp"

namespace agl
{

class	Scene;
class	TexturedComponentVAO;

class	TexturedComponent : public SceneComponent, public Renderable
{
private:
	TexturedComponentShaderProgram		_shaderProgram;
	TexturedComponentVAO				*_vao;

protected:
	virtual void		onModif(void);

public:
	TexturedComponent(Scene &scene, Area *area, const std::string &path,
			const Vec2 &position, const Vec2 &size,
			const PositionType pos_type = PositionType::POS_NORMAL,
			const PositionType size_type = PositionType::POS_NORMAL,
			const int level = 3)
			throw(ShaderProgram::InvalidShaderProgramException,
					Texture::InvalidFileException);
	~TexturedComponent(void);

	virtual void		render(void) const;

	virtual void		objectPrint(std::ostream &o) const;
};

}

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   FontTexture.hpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-16 18:14:30 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:30:41 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FONT_TEXTURE_HPP
# define FONT_TEXTURE_HPP

# include "Texture.hpp"

namespace agl
{

class	FontTexture : public Texture
{
private:
	unsigned int			*_char_length;
	unsigned int			*_char_x;
	const unsigned int		_start;
	const unsigned int		_nb;
	const unsigned int		_char_width;
	const unsigned int		_char_height;

	void			load_char_length(void);

public:
	FontTexture(const std::string &path, const unsigned int char_width, const unsigned int char_height,
			const unsigned int start, const unsigned int nb) throw(InvalidFileException);
	~FontTexture(void);

	unsigned int	getCharLength(const unsigned char c) const;
	unsigned int	getCharX(const unsigned char c) const;

	unsigned int	getCharWidth(void) const;
	unsigned int	getCharHeight(void) const;

	unsigned int	getCharId(const unsigned char c) const;

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

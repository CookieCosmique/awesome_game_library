/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Inputs.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-19 14:49:47 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:37:30 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INPUTS_HPP
# define INPUTS_HPP

# include <map>
# include <string>
# include <vector>
# include <list>
# include "Controllable.hpp"
# include "CursorMoveControllable.hpp"
# include "Object.hpp"

namespace agl
{

class	Inputs : public Object
{
private:
	double			_cursorPosX;
	double			_cursorPosY;
	
	std::map< int, std::vector<std::string> >			_keyActions;
	std::map< int, std::vector<std::string> >			_mouseButtonActions;
	std::map< std::string, std::list<Controllable*> >	_controllables;
	std::list<CursorMoveControllable*>					_cursorMoveControllables;

	void		linkMouseButtonAction(const int button, const int action,
			const std::string &name);
	void		unlinkMouseButtonAction(const int button, const int action);

public:
	Inputs(void);
	~Inputs(void);

	void		linkKeyAction(const int key, const int action,
			const std::string &name);
	void		unlinkKeyAction(const int key, const int action);
	void		addControllable(const std::string &action,
			Controllable *controllable);
	void		removeControllable(const std::string &action,
			Controllable *controllable);
	void		removeControllables(const std::string &action);

	void		addCursorMoveControllable(CursorMoveControllable *controllable);
	void		removeCursorMoveControllable(CursorMoveControllable *controllable);

	void		registerKeyAction(const int key, const int action,
			const std::string &name, Controllable *controllable);
	void		unregisterKeyAction(const int key, const int action,
			const std::string &name, Controllable *controllable);

	void		keyEvent(const int key, const int action);
	void		mouseButtonEvent(const int button, const int action);
	void		cursorMoveEvent(const double xpos, const double ypos);

	const std::string	getKeyActionName(const int key, const int action) const;
	const std::string	getMouseButtonActionName(const int button,
			const int action) const;
	void				getCursorPos(double *x, double *y) const;

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

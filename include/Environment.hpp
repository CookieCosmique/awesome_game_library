/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Environment.hpp                                 |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 03:39:04 by Bastien             |\________\          */
/*   Updated: 2016/06/04 16:26:54 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENVIRONMENT_HPP
# define ENVIRONMENT_HPP

# include "OpenGL.h"
# include "Object.hpp"
# include "Inputs.hpp"

/** \brief Define Screen ratio */
# define RATIO (16.0f / 9.0f)
/** \brief Define Screen height */
# define HEIGHT 720
/** \brief Define screen width according to screen ratio and screen height
*	\sa RATIO
*	\sa HEIGHT
*/
# define WIDTH (RATIO * HEIGHT)
/** \brief Define if fullscreen is enabled
*	0 for disabled
*	1 for enabled
*/
# define FULLSCREEN 0

namespace agl
{

/**	\brief The main class of the project (singleton)
*
*	Initialize flags, OpenGL, inputs and create the window and
*	the game
*/
class	Environment : public Object
{
public:
	/** \brief Exception when an error occurs on window creation */
	class	WindowCreationErrorException : public std::exception
	{
	public:
		virtual const char	*what(void) const throw();
	};
	/** \brief Exception when an error occurs on GLFW initialization */
	class	GLFWInitErrorException : public std::exception
	{
	public:
		virtual const char	*what(void) const throw();
	};
	/** \brief Exception when an error occurs on glew initialization */
	class	GlewInitErrorException : public std::exception
	{
	private:
		const char	*_error;
	public:
		GlewInitErrorException(const char *error);
		virtual const char	*what(void) const throw();
	};

private:
	GLFWwindow		*_window;	/**< \brief The window used, powered by glfw */
	Inputs			*_inputs;	/**< \brief The input handler */

	/**	\brief init window */
	void	initWindow(void) 	throw(GLFWInitErrorException,
			WindowCreationErrorException);
	/**	\brief init window */
	void	initGlew(void) 		throw(GlewInitErrorException);
	/**	\brief Used by init window ( will check for the most recent gl version) */
	void	createWindow(int width, int height, const char *title,
		GLFWmonitor *monitor, GLFWwindow *share);

	/**	\brief Private Environment constructor */
	Environment(void);

public:
	/**	\brief Environment destructor */
	~Environment(void);

	/**	\brief Initialize environment
	*	\param ac number of arguments
	*	\param av string array
	*	This function has to be called at the beggining.
	*/
	void				init(void) throw(GLFWInitErrorException,
			WindowCreationErrorException);

	/**	\brief function called when a key event occurs
	*	\param key is the key
	*	\param action can be GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT
	*/
//	void				keyEvent(const int key, const int action);
	/**	\brief Static function to get an instance of Environment */
	static Environment	&getInstance(void);

	/**	\brief return window */
	GLFWwindow		*getWindow(void) const;
	/**	\brief return inputs */
	Inputs			&getInputs(void) const;

	/** \brief object print */
	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

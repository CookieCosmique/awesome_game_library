/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Object.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 09:30:56 by Bastien             |\________\          */
/*   Updated: 2016/06/01 12:41:00 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OBJECT_HPP
# define OBJECT_HPP

# include <iostream>
# include <sstream>

namespace agl
{
	/**	\brief Object abstract class to debug draw itself
	*
	*	All other object will inherit from Object, and 
	*	will have to overwrite the toString function
	*/
	class	Object
	{

	public:
		virtual ~Object();
		/** \brief Output a String with the object info
		*
		*	This is the function that will be overwritten by 
		*	all the inherited classes of Object.
		*/
		virtual void	objectPrint(std::ostream &o) const = 0;
	};

	/** \brief the stream operator overload */
	std::ostream	&operator<<(std::ostream &o, const agl::Object &rhs);
}

#endif

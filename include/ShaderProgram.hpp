/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   ShaderProgram.hpp                               |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 10:49:46 by Bastien             |\________\          */
/*   Updated: 2016/06/14 17:37:46 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHADER_PROGRAM_HPP
# define SHADER_PROGRAM_HPP

# include <fstream>
# include "OpenGL.h"
# include "Object.hpp"

namespace agl
{

class	SourceShaderProgram;

class	ShaderProgram : public Object
{
private:
	GLuint				_idVertexShader;
	GLuint				_idGeometryShader;
	GLuint				_idFragmentShader;
	const std::string	_vertexShaderPath;
	const std::string	_geometryShaderPath;
	const std::string	_fragmentShaderPath;

	GLchar		*load_file(const char *path) const;
	int			compileShader(const GLchar *source, GLenum shader_type) const;
	int			loadShader(GLchar *source, GLenum shader_type) const;
	int			loadShader(const std::string &source, GLenum shader_type) const;

protected:
	GLuint				_id;

public:
	class	InvalidShaderProgramException : public std::exception
	{
	public:
		virtual const char	*what(void) const throw();
	};

	ShaderProgram(const SourceShaderProgram &sources)
		throw(InvalidShaderProgramException);
	ShaderProgram(const char *vertex_shader_path,
					const char *geometry_shader_path,
					const char *fragment_shader_path)
		throw(InvalidShaderProgramException);
	virtual ~ShaderProgram(void);

	void			use(void) const;
	
	GLuint			getID(void) const;

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

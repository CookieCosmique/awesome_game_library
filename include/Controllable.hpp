/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Controllable.hpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-19 14:50:33 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:34:33 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTROLLABLE_HPP
# define CONTROLLABLE_HPP

# include <string>
# include "Object.hpp"

namespace agl
{

class	Inputs;

class	Controllable : public virtual Object
{
protected:
	Inputs		&_inputs;

public:
	Controllable(Inputs &inputs);
	virtual ~Controllable(void);
	virtual void	input(const std::string &str) = 0;
};

}

#endif

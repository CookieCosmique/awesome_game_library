/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Update.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 12:25:23 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UPDATE_HPP
# define UPDATE_HPP

# include <list>
# include <forward_list>
# include "Object.hpp"
# include "Updatable.hpp"
# include "SmartPointer.hpp"

/** \brief The max number of different update step
*	Updates are done in steps, the array of deque is
*	updated square per square
*/
# define MAX_UPDATE_LEVEL 10

namespace agl
{
/** \brief Update will call the update functions of 
*	all updatable
*
*	Update will call the update function of all 
*	updatable created that are still alive, and will also 
*	delete the killable object
*/ 
class Update : public Object
{
private:
	std::forward_list< SmartPointer<Updatable> >		_updatableObjects[MAX_UPDATE_LEVEL]; /**< The array of lists of updatable object */

public:
	/** \brief Update constructor */
	Update(void);
	/** \brief Update destructor */
	~Update(void);

	/** \brief will call all the _updatableObjects 
	*	update function 
	*	\param delta_time the time span since last frame
	*/
	void	update(const GameTime &game_time);
	/**	\brief add a pointer to updatable in the _uptatableObjects[level] list
	*	\param *updatable the object to add a reference to
	*	\param level the level of update of the object, wich lists it will be added too
	*/
	void	addUpdatable(Updatable *updatable, const int level);

	/** \brief print object */
	virtual void	objectPrint(std::ostream &o) const;
};
}

#endif

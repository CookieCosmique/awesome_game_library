/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Updatable.hpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 12:23:52 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UPDATABLE_HPP
# define UPDATABLE_HPP

# include "Spawnable.hpp"
# include "GameTime.hpp"

namespace agl
{
class	Update;

/** An Updatable object, its update function will be called every frame */
class	Updatable : public virtual Spawnable
{
public:
	/** \brief Updatable constructor */
	Updatable(Update &update, const int level);
	/** \brief Updatable destructor */
	virtual ~Updatable();

	/** \brief The Update function to overwrite for children object */
	virtual void	update(const GameTime &game_time) = 0;
	
	/** \brief Print object */
	virtual void	objectPrint(std::ostream &o) const;
};
}

#endif


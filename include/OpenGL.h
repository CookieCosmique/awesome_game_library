/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   OpenGL.h                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-08 08:42:28 by Bastien             |\________\          */
/*   Updated: 2016/06/04 15:47:35 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPENGL_H
# define OPENGL_H

# ifdef __APPLE__
# 	define GLFW_INCLUDE_GLCOREARB
# endif
# if defined(_WIN32) || defined(__CYGWIN__)
# 	include <windef.h>
#	define GLEW_STATIC
# 	include "GL/glew.h"
# endif

# include "GLFW/glfw3.h"

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   InputHandler.hpp                                |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-01 02:34:18 by Bastien             |\________\          */
/*   Updated: 2016/06/04 15:51:29 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INPUT_HANDLER_HPP
# define INPUT_HANDLER_HPP

# include "OpenGL.h"

namespace agl
{

class	InputHandler
{
private:
	static void		keyCallback(GLFWwindow *window, int key, int scancode,
			int action, int mods);
	static void		mouseButtonCallback(GLFWwindow *window, int button,
			int action, int mods);
	static void		cursorPositionCallback(GLFWwindow *window, double xpos,
			double ypos);
public:
	InputHandler(GLFWwindow *window);
	~InputHandler(void);
};

}

#endif

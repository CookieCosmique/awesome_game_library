/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MouseButtonControllable.hpp                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/09 15:03:29 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/02 16:52:24 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MOUSE_BUTTON_CONTROLLABLE_HPP
# define MOUSE_BUTTON_CONTROLLABLE_HPP

# include <map>
# include "Controllable.hpp"
# include "Inputs.hpp"
# include "Logger.hpp"

namespace agl
{

template <typename T>
class	MouseButtonControllable : public Controllable
{
private:
	T			&_object;
	std::map< std::string, void (T::*)(void) >	_actions;

protected:
	void		linkMouseAction(const std::string &action,
			void (T::*fct)(void))
	{
		this->_actions[action] = fct;
		this->_inputs.addControllable(action, this);
	}
	void		unlinkMouseAction(const std::string &action)
	{
		this->_actions[action] = NULL;
		this->_inputs.removeControllable(action, this);
	}

	virtual bool	collide(const double x, const double y) const = 0;

public:
	MouseButtonControllable(Inputs &inputs, T &object) :
		Controllable(inputs),
		_object(object) {}
	virtual ~MouseButtonControllable(void) {}

	void		input(const std::string &str)
	{
		double		x;
		double		y;
		void		(T::*fct)(void);

		this->_inputs.getCursorPos(&x, &y);
		if (collide(x, y) && (fct = this->_actions[str]))
			(this->_object.*fct)();
	}

	virtual void	objectPrint(std::ostream &o) const
	{
		o << C_OBJ << "MouseButtonControllable" << C_RESET << ": {" <<
			logger::tab(1);
		o << logger::tab(-1) << "}";
	}
};

}

#endif

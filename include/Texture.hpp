/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Texture.hpp                                     |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-14 14:50:31 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:28:34 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXTURE_HPP
# define TEXTURE_HPP

# include "Object.hpp"
# include "OpenGL.h"

namespace agl
{

class	Texture : public Object
{
public:
	class	InvalidFileException : public std::exception
	{
		public:
			virtual const char		*what(void) const throw();
	};

private:
	GLuint				_id;
	
	void	loadBMP(const std::string &path) throw(InvalidFileException);

protected:
	const std::string	_path;
	unsigned int		_width;
	unsigned int		_height;
	unsigned int		_size;
	unsigned char		*_data;

public:
	Texture(const std::string &path) throw(InvalidFileException);
	~Texture(void);

	void			bind(void) const;

	unsigned int	getWidth(void) const;
	unsigned int	getHeight(void) const;

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Camera.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/07 11:32:19 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/01 12:11:19 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CAMERA_HPP
# define CAMERA_HPP

# include "Vec2.hpp"
# include "Spawnable.hpp"

namespace agl
{
class	Camera : public virtual Spawnable
{
protected:
	Vec2	_position;
	Vec2	_size;
public:
	Camera(const Vec2 &position, const Vec2 &size);
	~Camera(void);

	const Vec2		&getPosition(void) const;
	const Vec2		&getSize(void) const;

	virtual void	objectPrint(std::ostream &o) const;
};
}

#endif

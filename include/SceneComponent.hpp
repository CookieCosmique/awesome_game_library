/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   SceneComponent.hpp                              |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 15:06:41 by Bastien             |\________\          */
/*   Updated: 2016/06/14 14:59:18 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCENE_COMPONENT_HPP
# define SCENE_COMPONENT_HPP

# include "Vec2.hpp"
# include "SmartPointer.hpp"
# include "Spawnable.hpp"
# include "Scene.hpp"

namespace agl
{

class	Area;

class	SceneComponent : public virtual Spawnable
{
public:
	enum	PositionType
	{
		POS_NORMAL,
		POS_RATIO
	};

private:
	Vec2	getRatio(const Vec2 &ratio) const;
	void	setPositionRatio(const Vec2 &position, const Vec2 &size,
			const Vec2 &ratio);
	void	initPositionRatio(const Vec2 &position, const Vec2 &size,
			const Vec2 &ratio);
	void	initPosition(const Vec2 &position, const PositionType type);
	void	initSize(const Vec2 &size, const PositionType type);

protected:
	Area			*_area;

	Scene			&_scene;

	Vec2			_position;
	Vec2			_size;

	virtual void		onModif(void) = 0;

public:
	SceneComponent(Scene &scene, Area *area,
			const Vec2 &position, const Vec2 &size,
			const PositionType pos_type = PositionType::POS_NORMAL,
			const PositionType size_type = PositionType::POS_NORMAL);
	virtual ~SceneComponent(void);

	virtual void		scale(const float x, const float y);
	void				scale(const Vec2 &coef);
	void				scale(const float coef);
	void				scaleArea(const float x, const float y);
	void				scaleArea(const Vec2 &coef);
	void				scaleArea(const float coef);
	virtual void		translate(const float x, const float y);
	void				translate(const Vec2 &vec);

	void				setPosition(const Vec2 &position,
			const PositionType type = PositionType::POS_NORMAL);
	void				setSize(const Vec2 &size,
			const PositionType type = PositionType::POS_NORMAL);

	const Vec2			&getPosition(void) const;
	const Vec2			&getSize(void) const;

	virtual void		objectPrint(std::ostream &o) const;
};

}

#endif

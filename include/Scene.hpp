/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Scene.hpp                                       |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-06 13:07:17 by Bastien             |\________\          */
/*   Updated: 2016/06/01 16:44:18 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCENE_HPP
# define SCENE_HPP

# include "Object.hpp"
# include "SmartPointer.hpp"
# include "OpenGL.h"

namespace agl
{
class	Update;
class	Render;
class	GameTime;
class	Camera;

class	Scene : public virtual Object
{
private:
	GLFWwindow		*_window;
	bool			_running;

protected:
	Update					*_update;
	Render					*_render;
	GameTime				*_time;
	SmartPointer<Camera>	_sceneCamera;

public:
	Scene(GLFWwindow *window);
	Scene(GLFWwindow *window, Update *update, Render *render);
	~Scene(void);

	virtual int		run(void);
	void			stop(void);

	bool			isRunning(void) const;
	Update			&getUpdate(void);
	Render			&getRender(void);
	GameTime		&getTime(void);
	Camera			&getCamera(void);

	virtual void	objectPrint(std::ostream &o) const;
};
}

#endif

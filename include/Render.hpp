/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Render.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 03:39:04 by Bastien             |\________\          */
/*   Updated: 2016/06/01 12:14:14 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RENDER_HPP
# define RENDER_HPP

# include <forward_list>
# include "OpenGL.h"
# include "Object.hpp"
# include "Renderable.hpp"
# include "SmartPointer.hpp"

# define MAX_RENDER_LEVEL 10

namespace agl
{
class	Render : public Object
{
private:
	std::forward_list< SmartPointer<Renderable> >	_renderableObjects[MAX_RENDER_LEVEL];

public:
	Render(void);
	~Render(void);

	void	render(GLFWwindow *window);
	void	addRenderable(Renderable *renderable, const int level);

	/** \brief print object */
	virtual void	objectPrint(std::ostream &o) const;
};
}

#endif

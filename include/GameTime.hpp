/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   GameTime.hpp                                    |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-27 16:48:44 by Bastien             |\________\          */
/*   Updated: 2016/06/01 12:17:06 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_TIME_HPP
# define GAME_TIME_HPP

# include "Object.hpp"

namespace agl
{
/**	\brief The GameTime class manage the time and the speed of the game
*
*	It calculate the time between 2 frames, the fps and contains the game timer
*	Using glfwGetTime() function
*/
class	GameTime : public Object
{
private:
	double		_time;				/**< \brief the game timer */
	double		_last_time;			/**< \brief the time of the last frame */
	double		_time_diff;			/**< \brief the time difference between two frames */
	double		_speed;				/**< \brief the speed of the game */
	double		_time_diff_refresh; /**< \brief the time after the last refresh */
	float		_refresh;			/**< \brief the time between two refreshes */
	int			_frames;			/**< \brief the number of frames after the last refresh */

	/**	\brief get a string to print the time
	*
	*	format: "MM:SS:DD" (D = hundredth)
	*	\param t the time (in seconds)
	*/
	std::string		timeToString(const double t) const;
public:
	/** \brief GameTime base constructor
	*
	*	call the \b reset() member function
	*/
	GameTime(void);
	/** \brief GameTime base destructor */
	~GameTime(void);
	/**	\brief update the time of the game
	*
	*	calculate \c _time_diff \n
	*	add \c time_diff * \c speed to \c time \n
	*	increments \c frames \n
	*	add \c time_diff to \c time_diff_refresh \n
	*	print fps if \c time_diff_refresh >= \c refresh
	*/
	void			update(void);
	/**	\brief reset/initialize the time
	*
	*	set \c time to 0, \c speed to 1, \c refresh to 1
	*/
	void			reset(void);
	/** \brief return \c time */
	double			getTime(void) const;
	std::string		getTimeStr(void) const;
	/** \brief return \c time_diff */
	double			getDiff(void) const;
	/** \brief return \c speed */
	double			getSpeed(void) const;
	/** \brief set \c speed */
	void			setSpeed(const double speed);
	bool			isRefreshing(void) const
	{
		return (this->_time_diff_refresh == 0);
	}

	virtual void	objectPrint(std::ostream &o) const;
};
}

#endif

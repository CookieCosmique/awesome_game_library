/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   CursorMoveControllable.hpp                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 12:16:33 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:35:47 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CURSOR_MOVE_CONTROLLABLE_HPP
# define CURSOR_MOVE_CONTROLLABLE_HPP

# include "Object.hpp"

namespace agl
{

class	Inputs;

class	CursorMoveControllable : public virtual Object
{
protected:
	Inputs		&_cursor_inputs;

	void		enable(void);
	void		disable(void);
	
public:
	CursorMoveControllable(Inputs &inputs);
	~CursorMoveControllable(void);

	virtual void	cursorMove(const double x, const double y) = 0;
};

}

#endif

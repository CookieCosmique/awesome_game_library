/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   SquareVAO.hpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-14 21:59:40 by Bastien             |\________\          */
/*   Updated: 2016/06/16 15:18:27 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUARE_VAO_HPP
# define SQUARE_VAO_HPP

# include "VAO.hpp"
# include "Vec2.hpp"

namespace agl
{

class	SquareVAO : public VAO
{
private:
	GLuint		_vbo;
	GLuint		_ebo;

public:
	SquareVAO(const GLuint program_id, const Vec2 &position, const Vec2 &size);
	virtual ~SquareVAO(void);

	virtual void	render(void) const;

	void			updatePositions(const Vec2 &position, const Vec2 &size);

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

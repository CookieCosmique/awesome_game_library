/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   VAO.hpp                                         |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-31 06:15:05 by Bastien             |\________\          */
/*   Updated: 2016/06/02 17:02:43 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VAO_HPP
# define VAO_HPP

# include "OpenGL.h"
# include "Object.hpp"

namespace agl
{

class	VAO : public Object
{

private:
	GLuint		_vao;

protected:
	void		bind(void) const;
	GLuint		createVBO(const GLuint program_id, GLenum type,
			const GLuint index, const char *name, GLint nb) const;
	GLuint		createEBO(void) const;
	GLuint		loadVBO(const GLuint program_id, GLsizeiptr size,
			const GLvoid *data, GLenum type, const GLuint index,
			const char *name, GLint nb, GLenum usage) const;
	GLuint		loadEBO(GLsizeiptr size, const GLvoid *data, GLenum usage) const;
	void		updateBufferData(GLenum target, GLuint buffer, GLsizeiptr size,
			const GLvoid *data) const;

public:
	VAO(void);
	virtual	~VAO(void);

	virtual void	render(void) const = 0;

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

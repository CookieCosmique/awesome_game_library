/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   KeyboardControllable.hpp                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-04-20 07:00:01 by Bastien             |\________\          */
/*   Updated: 2016/06/02 16:53:26 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYBOARD_CONTROLLABLE_HPP
# define KEYBOARD_CONTROLLABLE_HPP

# include <string>
# include <map>
# include "Controllable.hpp"
# include "Inputs.hpp"
# include "Logger.hpp"

namespace agl
{

template<typename T>
class	KeyboardControllable : public Controllable
{
private:
	T		&_object;
	std::map<std::string, void (T::*)(void)>	_actions;

protected:
	void		linkInput(const int key, const int action, std::string name, void (T::*fct)(void))
	{
		this->_actions[name] = fct;
		this->_inputs.registerKeyAction(key, action, name, this);
	}
	void		unlinkInput(const int key, const int action, std::string name)
	{
		this->_actions[name] = NULL;
		this->_inputs.unregisterKeyAction(key, action, name, this);
	}

public:
	KeyboardControllable(Inputs &inputs, T &object) : Controllable(inputs), _object(object) {}
	virtual ~KeyboardControllable(void) {}

	void		input(const std::string &str)
	{
		void	(T::*fct)(void);

		if ((fct = this->_actions[str]))
			(this->_object.*fct)();
	}

	virtual void	objectPrint(std::ostream &o) const
	{
		typename std::map<std::string, void (T::*)(void)>::const_iterator	it = this->_actions.begin();

		o << C_OBJ << "KeyboardControllable" << C_RESET << ": {" << logger::tab(1);
		o << C_VAR << "actions" << C_RESET << " = {" << logger::tab(1);
		while (it != this->_actions.end())
		{
			o << C_STR << it->first << C_RESET;
		if (++it != this->_actions.end())
			o << ",\n";
		}
		o << logger::tab(-1) << "}";
		o << logger::tab(-1) << "}";
	}
};

}

#endif

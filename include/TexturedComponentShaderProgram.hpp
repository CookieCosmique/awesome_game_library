/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TexturedComponentShaderProgram.hpp                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/14 16:05:52 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/16 17:12:42 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXTURED_COMPONENT_SHADER_PROGRAM_HPP
# define TEXTURED_COMPONENT_SHADER_PROGRAM_HPP

# include "CameraShaderProgram.hpp"

namespace agl
{

class	TexturedComponentShaderProgram : public CameraShaderProgram
{
public:
	TexturedComponentShaderProgram(void) throw(InvalidShaderProgramException);
	~TexturedComponentShaderProgram(void);

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

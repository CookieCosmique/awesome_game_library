/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Vec2.hpp                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 12:50:48 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VEC2_HPP
# define VEC2_HPP

# include <ostream>

namespace agl
{
/** \brief A bidimensionnal vector
*	
*	A basic 2-dimension vector 
*	utility class
*/
class Vec2
{
private:
	float	_xy[2];/**< the X : [0] and Y : [1] coordinate */

public:
	float	&x;
	float	&y;

public:
	/** \brief Void constructor */
	Vec2(void);
	/** \brief Constructor from x and y */
	Vec2(float x, float y);
	/** \brief Constructor from a pointer on 2float in a row */
	Vec2(float *xy);
	/** \brief Copy Constructor */
	Vec2(const Vec2 &other);

	/** \brief will return the pointer to the _xy[] member */
	const float	*getXY(void) const;
	/** \brief will return the length ( or norm) of the vector */
	float	length(void) const;
	/** \brief the X vector projection */
	Vec2	vx(void) const;
	/** \brief the Y vector projection */
	Vec2	vy(void) const;
	/** \brief return TRUE if the vector is the (0,0) vector */
	bool	isNull(void) const;
	/** \brief set the X value of the vector */
	void	setX(const float x);
	/** \brief set the Y value of the vector */
	void	setY(const float y);

	/** \brief will modify the vector so its length is 1
	*
	*	Will do nothing if the vector is (0,0)
	*/
	Vec2	&normalize(void);
	/** \brief will shift the vector by pi/2
	*
	*	the shift is in the trigonometrique direction
	*	, counter clockwise
	*/
	Vec2	&shift(void);
	/** \brief will shift the vector by -pi/2
	*
	*	the shift is in the opposite trigonometrique direction
	*	, clockwise
	*/
	Vec2	&shiftX(void);
	/** \brief Rotate the vector of an angle theta, in radians */
	Vec2	&rotate(const float theta);

	/** \brief Return a new vector, of length 1 with the same direction
	*
	*	if this vector is (0,0), will return the (0,0) vector
	*/
	Vec2	tangent(void) const;
	/** \brief same as tangent */
	Vec2	normalized(void) const;
	/** \brief Return a new vector, of length 1 normal to this vector 
	*	\param theta The angle by wich the vector will be rotated
	*/
	Vec2	normal(void) const;
	/** \brief Return a new vector, corresponding of this one rotated by theta 
	*	\param theta The angle by wich the vector will be rotated
	*/
	Vec2	rotated(const float theta) const;

	/** \brief Vectorial + */
	Vec2		operator+(const Vec2 &other) const;
	/** \brief Vectorial - */
	Vec2		operator-(const Vec2 &other) const;
	/** \brief Return the negative vector */
	Vec2		operator-(void) const;
	Vec2		operator/(const Vec2 &other) const;
	Vec2		operator/(const float coef) const;
	/** \brief Vectorial dot product */
	float		operator*(const Vec2 &other) const;
	/** \brief Vectorial cross product */
	float		operator^(const Vec2 &other) const;
	/** \brief Return the closest multiple of other to this vector */
	Vec2		operator%(const Vec2 &other) const;
	/** \brief Per coordinate = */
	Vec2		&operator=(const Vec2 &other);
	/** \brief Set the vector to this + other */
	Vec2		&operator+=(const Vec2 &other);
	/** \brief Set the vector to this - other */
	Vec2		&operator-=(const Vec2 &other);
	/** \brief Set the vector to this * scalar */
	Vec2		&operator*=(const float scalar);
	/** \brief Scalar * Vector operator
	*	Will have the same behavior as Vector * Scalar
	*/
	friend Vec2	operator*(const float scalar, const Vec2 &vec);
	/** \brief Vector * Scalar operator
	*	Will have the same behavior as Scalar * Vector
	*/
	friend Vec2	operator*(const Vec2 &vec, const float scalar);
	/** \brief Scalar * Vector operator
	*	Will have the same behavior as Vector * Scalar
	*/
	friend Vec2	operator*(const double scalar, const Vec2 &vec);
	/** \brief Vector * Scalar operator
	*	Will have the same behavior as Scalar * Vector
	*/
	friend Vec2	operator*(const Vec2 &vec, const double scalar);
		/** \brief Scalar * Vector operator
	*	Will have the same behavior as Vector * Scalar
	*/
	friend Vec2	operator*(const int scalar, const Vec2 &vec);
	/** \brief Vector * Scalar operator
	*	Will have the same behavior as Scalar * Vector
	*/
	friend Vec2	operator*(const Vec2 &vec, const int scalar);

	/** \brief Simple per coordinate equality test oprator */
	bool		operator==(const Vec2 &other) const;

	/** \brief Extern Tangent
	*	\sa tangent
	*/
	friend Vec2	tangent(const Vec2 &vec);
	/** \brief Extern Normal
	*	\sa normal
	*/
	friend Vec2	normal(const Vec2 &vec);
	/** \brief Extern rotated
	*	\sa rotated
	*/
	friend Vec2	rotated(const Vec2 &vec, const float theta);

	/** \brief << operator */
	friend std::ostream	&operator<<(std::ostream &o, const Vec2 &vec);

};

namespace	vec2
{
	extern Vec2			up;
	extern Vec2			down;
	extern Vec2			right;
	extern Vec2			left;
	extern Vec2			null;

	extern Vec2			x;
	extern Vec2			y;
	
	float	moded(float a, float b);
	float	moduled(float a, float b);
	int		floor(float a);
}

}

#endif

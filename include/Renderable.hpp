/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Renderable.hpp                                  |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 03:39:04 by Bastien             |\________\          */
/*   Updated: 2016/06/01 12:06:27 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RENDERABLE_HPP
# define RENDERABLE_HPP

# include "Spawnable.hpp"
# include "Camera.hpp"

namespace agl
{
class	Render;

/** \brief A Renderable object, his function render will be called every frame */
class	Renderable : public virtual Spawnable
{
protected:
	Camera		&_camera;		/**< \brief Camera reference */

public:
	/** \brief Constructor
	*
	*	Spawn the Renderable in game using game.spawn()
	*/
	Renderable(Render &render, Camera &camera, const int level);
	/** \brief Destructor */
	virtual	~Renderable();

	/** \brief The render() function to overwrite for children object */
	virtual void	render(void) const = 0;
	
	/** \brief Print object */
	virtual void	objectPrint(std::ostream &o) const;
};
}

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   CameraShaderProgram.hpp                         |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-16 17:06:48 by Bastien             |\________\          */
/*   Updated: 2016/06/16 15:28:05 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CAMERA_SHADER_PROGRAM_HPP
# define CAMERA_SHADER_PROGRAM_HPP

# include "ShaderProgram.hpp"

namespace agl
{

class	Camera;

class	CameraShaderProgram : public ShaderProgram
{
private:
	GLuint		_id_cam_pos;
	GLuint		_id_cam_size;

public:
	CameraShaderProgram(const SourceShaderProgram &sources)
		throw(InvalidShaderProgramException);
	CameraShaderProgram(const char *vertex_shader_path,
					const char *geometry_shader_path,
					const char *fragment_shader_path)
		throw(InvalidShaderProgramException);
	virtual ~CameraShaderProgram(void);

	void		updateCamera(const Camera &camera) const;

	virtual void	objectPrint(std::ostream &o) const;

};

}

#endif

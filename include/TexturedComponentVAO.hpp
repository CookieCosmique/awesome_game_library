/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   TexturedComponentVAO.hpp                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-14 15:40:39 by Bastien             |\________\          */
/*   Updated: 2016/06/16 17:22:10 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXTURED_COMPONENT_VAO_HPP
# define TEXTURED_COMPONENT_VAO_HPP

# include "Vec2.hpp"
# include "SquareVAO.hpp"
# include "Texture.hpp"

namespace agl
{

class	TexturedComponentVAO : public SquareVAO
{
private:
	GLuint			_vbo_uv;
	Texture			*_texture;

public:
	TexturedComponentVAO(const GLuint program_id, const Vec2 &position,
			const Vec2 &size, const std::string &texture_path)
		throw(Texture::InvalidFileException);
	~TexturedComponentVAO(void);

	virtual void	render(void) const;

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

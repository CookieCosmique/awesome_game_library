/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Spawnable.hpp                                   |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 14:02:35 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 12:44:07 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAWNABLE_HPP
# define SPAWNABLE_HPP

#include "Object.hpp"

namespace agl
{
/**	\brief A Spawnable object
*
*	_referenced will acount for the number
*	of reference to this object, if the value is
*	1 , and the object have to be killed , you can
*	kill it.
*/
class Spawnable : public virtual Object
{

private:
	int			_references; /**< \brief the number of pointer to this object */
	bool		_toKill; /**< \brief is the object dead or not */

public:
	/**	\brief Simple Constructor
	*	set _references to 0 and _toKill to false
	*/
	Spawnable(void);
	/**	\brief Simple Constructor
	*	set _references to 0 and _toKill to false
	*/
	virtual ~Spawnable(void);

	/** \brief kill the object
	*
	*	Set the object _toKill to true
	*	and return true if the object is 
	*	killable , false otherwise
	*/	
	bool 	kill(void);
	/** \brief return true if the object is alive */
	bool	isAlive(void) const;
	/** \brief Can the object be killed
	*
	*	The object can be killed if _references
	*	is equal to 0 (or less)
	*/
	bool	isKillable(void) const;
	/** \brief ++ operator to tell the object you add a reference
	*	
	*	When you add a pointer to the object, you should call the ++
	*	operator
	*/
	Spawnable &operator++(int);
	/** \brief -- operator to tell the object you deleted a reference
	*	
	*	When you deletea pointer to the object, you should call the --
	*	operator
	*/
	Spawnable &operator--(int);
//	friend Spawnable &operator--(Spawnable &spawn, int);
	/** \brief Spawnable : object print overload << overwrite */
	virtual void	objectPrint(std::ostream &o) const;
};
}

#endif

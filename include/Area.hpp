/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Area.hpp                                        |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-05-13 14:50:27 by Bastien             |\________\          */
/*   Updated: 2016/06/14 14:35:58 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AREA_HPP
# define AREA_HPP

# include <forward_list>
# include "SceneComponent.hpp"

namespace agl
{

class	Area : public SceneComponent
{
private:
	std::forward_list< SmartPointer<SceneComponent> >	_componentList;

public:
	Area(Scene &scene, Area *area, const Vec2 &position, const Vec2 &size,
			const PositionType pos_type = PositionType::POS_NORMAL,
			const PositionType size_type = PositionType::POS_NORMAL);
	~Area(void);

	void			addComponent(SceneComponent *component);

	void			scale(const float x, const float y);
	void			translate(const float x, const float y);

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif

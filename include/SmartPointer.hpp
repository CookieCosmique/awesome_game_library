/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   SmartPointer.hpp 		                         |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Clhawliet                                        /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-17 12:26:30 by Clhawliet           |\________\          */
/*   Updated: 2016/06/01 12:14:02 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SMART_POINTER_HPP
# define SMART_POINTER_HPP

namespace agl
{
/**	\brief Smart pointer to avoid deletion problems
*
*	Store an object and manage the number of references of this object
*	using ++ and -- operators
*	\sa Spawnable
*/
template <typename T>
class SmartPointer
{
private:
	T	*_item;		/**< \brief	A pointer to the object stocked */

public:
	/** \brief Base constructor, initialize _item to NULL */
	SmartPointer(void)
	{
		this->_item = 0;
	}
	/** \brief Copy constructor, stock the item from other and add a reference */
	SmartPointer(const SmartPointer<T> &other) 
	{ 
		this->_item = other._item;
		if (this->_item)
			(*this->_item)++;
	}
	/**	\brief Stock the item from other and add a reference
	*
	*	item in other is not the same type
	*/
	template <typename Y>
	SmartPointer(const SmartPointer<Y> &other)
	{ 
		this->_item = other._item;
		if (this->_item)
			(*this->_item)++;
	}
	/** \brief Contructor, stock the item and add a reference */
	SmartPointer(T *item)
	{
		this->_item = item;
		if (this->_item)
			(*this->_item)++;
	}
	/**	\brief Remove a reference of item but do not delete it */
	~SmartPointer(void)
	{
		if (this->_item)
			(*this->_item)--;
	}

	/**	\brief Operator = overload
	*
	*	Change the value of _item
	*/
	template <typename Y>
	SmartPointer	&operator=(const SmartPointer<Y> &other)
	{
		if (this->_item)
			(*this->_item)--;
		this->_item = other._item;
		if (this->_item)
			(*this->_item)++;

		return (*this);
	}
	/** \brief operator = overload
	*
	*	Change the value of _item
	*/
	SmartPointer	&operator=(T *item)
	{	
		if (this->_item)
			(*this->_item)--;
		this->_item = item;
		if (this->_item)
			(*this->_item)++;

		return (*this);
	}

	bool			operator==(T *item)
	{
		return (this->_item == item);
	}

	bool			operator!=(T *item)
	{
		return (this->_item != item);
	}

	/** \brief operator * overload
	*
	*	get the item stored as a reference
	*/
	T	&operator*(void)
	{
		return (*this->_item);
	}
	/** \brief operator -> overload
	*
	*	access de stored pointer members
	*/
	T	*operator->(void)
	{
		return (this->_item);
	}
	/** \brief pointer implicit cast operator
	*	
	*	allow conversion from the smart pointer to
	*	the pointer type implicitly
	*/
	operator T*() const
	{
		return	(this->_item);
	}
};
}

#endif

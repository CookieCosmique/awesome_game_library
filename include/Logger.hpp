/* ************************************************************************** */
/*                                                                            */
/*                                                    ________                */
/*   Logger.hpp                                      |\_____  \               */
/*                                                    \|___/  /|              */
/*   By: Bastien                                          /  / /              */
/*                                                       /  /_/__             */
/*   Created: 2016-03-16 00:45:49 by Bastien             |\________\          */
/*   Updated: 2016/06/01 18:34:49 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LOGGER_HPP
# define LOGGER_HPP

# include <iostream>
# include <sstream>
# include <ctime>

# include "Object.hpp"

# define C_CLEAR ("\033[H\033[2J")
# define C_RESET ("\033[0m")
# define C_BOLD ("\033[1m")
# define C_REV ("\033[7m")

# define C_WHITE ("\x1B[0m")
# define C_RED ("\x1B[31m")
# define C_GREEN ("\x1B[32m")
# define C_YELLOW ("\x1B[33m")
# define C_BLUE ("\x1B[34m")
# define C_MAGENTA ("\x1B[35m")
# define C_CYAN ("\x1B[36m")
# define C_GRAY ("\033[22;37m")

# define C_BWHITE ("\033[1m\x1B[0m")
# define C_BRED ("\033[1m\x1B[31m")
# define C_BGREEN ("\033[1m\x1B[32m")
# define C_BYELLOW ("\033[1m\x1B[33m")
# define C_BBLUE ("\033[1m\x1B[34m")
# define C_BMAGENTA ("\033[1m\x1B[35m")
# define C_BCYAN ("\033[1m\x1B[36m")
# define C_BGRAY ("\033[1m\033[22;37m")

# define C_OBJ C_BBLUE
# define C_VAR C_YELLOW
# define C_STR C_CYAN

namespace agl
{
/** \brief Enum of all log types used in the \b Logger class
*	\sa Logger
*/
enum			e_logtype
{
	LOG_ERROR,		/**< [ERROR] */
	LOG_FINE,		/**< [FINE] */
	LOG_INFO,		/**< [INFO] */
	LOG_WARNING,	/**< [WARN] */
	LOG_DEBUG		/**< [DEBUG] */
};

/** \brief Enum of flags used in the \b Logger class
*/
enum			e_log_flags
{
	F_LOG_DEBUG = 1, /**< \brief Display debug messages */
	F_LOG_TIME = 2 /**< \brief Display time before messages */
};

/** \brief \b Logger class used to print messages in console
*
*	Can print 5 types of messages defined in \b e_logtype \n
*	Can add tabulations, type, and time before every message \n
*	\sa e_logtype
*	\sa e_log_flags
*/
class	Logger : Object
{

private:

	/** \brief structure used in Logger class to display types with color */
	typedef struct	s_log_format
	{
		char	str[8];
		char	color[16];
	}				t_log_format;

	static const t_log_format	_format[5];		/**< \brief Array with log types strings and colors */
	const int					_max_tabs;		/**< \brief Maximum tabulations before the message */
	int							_flags;			/**< \brief Log flags */
	int							_tabs;			/**< \brief Current number of tabulations before the message */
	std::ostringstream			_bufferStream;	/**< \brief the buffer that contains the current message */
	int							_bufferType;	/**< \brief the type of the message in the buffer */

	/** \brief Print the log message
	*
	*	Print the log type using \c format , print \c tabs tabulations
	*	and the message.
	*	Each line is treated independently
	*	\param type the type of the message
	*	\param s the string of the message
	*	\sa e_logtype
	*/
	void	print(const int type, const std::string &s) const;
	/** \brief return the max beetween _tabs and _max_tabs; */ //should i use it to have under 0 _tabs value ?
	int		currTab(void) const;
	std::string	getTimeString(void) const;
	bool	hasFlag(const int flag) const;
public:

	/** \brief Class used by Logger to print the buffer */
	class	EndLog
	{
	private:
		Logger			&_logger;
		const int		_tab;
	public:	
		EndLog(Logger &logger, const int tab);
		Logger	&getLogger(void) const;
		int		getTab(void) const;
	};
	/** \brief Class used by Logger to start a new line with 1 more tab */
	class	TabLog
	{
	private:
		Logger			&_logger;
		const int		_tab;
	public:	
		TabLog(Logger &logger, const int tab);
		Logger	&getLogger(void) const;
		int		getTab(void) const;
	};

	/**	\brief Logger constructor */
	Logger(void);
	/**	\brief Logger constructor
	*	\param flags define all flags
	*/
	Logger(const int flags);
	/**	\brief Logger destructor */
	virtual	~Logger(void);

	/**	\brief Print the message then add tabulations and return the type
	*
	*	\param type type of message
	*	\param s string of message
	*	\param tab number of tabulations added for next messages. default value is \c 0
	*/
	int		log(const int type, const std::string &s, const int tab = 0);

	/**	\brief Add tabulations for the next messages
	*	\param tab number of tabulations added (can be negative)
	*/
	void	addTab(const int tab);
	/**	\brief Set a number of tabulations for next messages
	*	\param tab number of tabulations set
	*/
	void	setTab(const int tab);
	/**	\brief Add a flag
	*	\param flag flag added
	*	\sa e_log_flags
	*/
	void	addFlag(const int flag);
	/**	\brief Remove a flag
	*	\param flag flag removed
	*	\sa e_log_flags
	*/
	void	removeFlag(const int flag);
	/**	\brief Set flags
	*	\param flags flags set
	*	\sa e_log_flags
	*/
	void	setFlags(const int flags);

	/**	\brief Create a new buffer
	*	\param type the type of the log
	*	\param tab the number of tabulation to add before the message	
	*/
	std::ostringstream	&sslog(const int type, const int tab);
	/**	\brief Create and return a new EndLog
	*	\param tab the number of tabulation to add after the message
	*/
	EndLog				endlog(const int tab);
	/**	\brief Create qnd return q new TabLog
	*	\param tab the number of tabulation to add
	*/
	TabLog				tablog(const int tab);
	/**	\brief Return current buffer type */
	int					getBufferType(void) const;
	/**	\brief Return current buffer stream */
	std::ostringstream	&getBufferStream(void);

	void				objectPrint(std::ostream &o) const;
	/** \brief operator << overload */
//	friend std::ostream	&operator<<(std::ostream &o, Logger &logger);

};

/** \brief operator << overload for EndLog */
void				operator<<(const std::ostream &os, const Logger::EndLog &el);
/** \brief operator << overload for TabLog */
std::ostringstream	&operator<<(const std::ostream &os, const Logger::TabLog &el);

/** \brief Logger namespace */
namespace	logger
{
	extern Logger	logger;	/**< \brief global logger class */

	/** \brief call the Logger::log() function */
	int					log(const int type, const std::string &s, const int _tab = 0);
	/** \brief call the Logger::sslog() function */
	std::ostringstream	&sslog(const int type, const int _tab = 0);
	/** \brief call the Logger::tab() function */
	Logger::TabLog		tab(const int _tab = 1);
	/** \brief call the Logger::endlog() function */
	Logger::EndLog		endlog(const int _tab = 0);
	/** \brief call the Logger::getBufferType() function */
	int					getBufferType(void);
	/** \brief call the Logger::getBufferStream() function */
	std::ostringstream	&getBufferStream(void);
}
}

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SourceShaderProgram.hpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 16:35:26 by bperreon          #+#    #+#             */
/*   Updated: 2016/06/14 17:22:50 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SOURCE_SHADER_PROGRAM_HPP
# define SOURCE_SHADER_PROGRAM_HPP

# include "Object.hpp"

namespace agl
{

class	SourceShaderProgram : public Object
{
private:
	const std::string	_vertex;
	const std::string	_geometry;
	const std::string	_fragment;

public:
	SourceShaderProgram(const std::string vertex, const std::string geometry,
			const std::string fragment);
	virtual ~SourceShaderProgram(void);

	const std::string	&getVertex(void) const;
	const std::string	&getGeometry(void) const;
	const std::string	&getFragment(void) const;

	virtual void	objectPrint(std::ostream &o) const;
};

}

#endif
